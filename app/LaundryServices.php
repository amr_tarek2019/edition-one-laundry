<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LaundryServices extends Model
{
    protected $guarded = ['id'];
    use SoftDeletes;
    protected $fillable = [

        'laundry_id',
        'name',
        'name_en',
        'wash',
        'ironing',
        'wash_ironing',
        'section_id',
        'img',
    ];


    public function getWashAttribute($value)
    {
        return $value ? $value : "";
    }

    public function getIroningAttribute($value)
    {
        return $value ? $value : "";
    }

    public function getWashIroningAttribute($value)
    {
//        $wash_ironing = intval($this->wash) + intval($this->ironing);
//        return $wash_ironing == 0 ? "" : $wash_ironing;
        return $value ? $value : "";

    }

    public function laundry()
    {
        return $this->belongsTo(Laundry::class);
    }

    public function getImgAttribute($value)
    {
        return $value ? asset('uploads/' . class_basename($this) . '/' . $value) : "";
    }

    public function setImgAttribute($value)
    {
        if (is_file($value)) {
            if ($value) {
                $name = rand(0000, 9999) . time() . '.' . $value->getClientOriginalExtension();
                $value->move(public_path('uploads/' . class_basename($this)), $name);
                $this->attributes['img'] = $name;
            }
        } else {
            $this->attributes['img'] = $value;
        }
    }
}
