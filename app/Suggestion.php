<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $fillable = [
        'user_id',
        'user_type',
        'title',
        'details',
    ];

    public function user()
    {
        return $this->morphTo();
    }
}
