<?php

namespace App\Http\Middleware;

use App\Delegate;
use Closure;

class ApiDelegateJWT
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('jwt'); // getting the token from the request header
        if (!$token) {
            return response(apiResponse(405, 'please enter jwt')); // checking if the token exists in the header
        } elseif ($user = Delegate::whereJwt($token)->first()) {  // checking if the token is correct
            if ($user->is_active == 0) {
                return response(apiResponse(405, 'تم تعليق الحساب من قبل الادمن')); // checking if the user is active
            } else
                return $next($request);
        }
        return response(apiResponse(405, 'please enter a valid jwt'));

    }
}
