<?php

namespace App\Http\Middleware;

use Closure;

class Laundry
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = auth()->guard('laundry')->user()) {
            if ($user->is_active == 0) {
                return back()->withErrors('تم تعليق الحساب من قبل الادمن');
            } else
                return $next($request);
        }
        return redirect()->route('laundry.auth.index');

    }
}
