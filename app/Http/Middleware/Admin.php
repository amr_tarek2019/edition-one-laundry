<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = auth()->user()) {
            if ($user->is_active == 0) {
                return back()->withErrors('تم تعليق الحساب من قبل الادمن');
            } else
                return $next($request);
        }
        return redirect()->route('admin.auth.index');

    }
}
