<?php

namespace App\Http\Middleware;

use Closure;

class isAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth('web')->user()) {
            return redirect()->route('site.auth.index');
        }
        return $next($request);
    }
}
