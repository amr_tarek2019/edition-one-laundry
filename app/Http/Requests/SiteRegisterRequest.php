<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'email' => 'required|max:50|unique:users,email',
            'password' => 'required_without:social_token|max:50',
            'phone' => 'required|max:50|unique:users,phone',
            'lat' => 'required|max:50',
            'lng' => 'required|max:50',
            'address' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'برجاء إدخال الإسم',
            'email.required' => 'برجاء إدخال البريد الإلكترونى',
            'email.unique' => ' البريد الإلكترونى مسجل بالفعل',
            'password.required' => ' برجاء إدخال كلمه المرور',
            'phone.required' => 'برجاء إدخال رقم الهاتف',
            'phone.unique' => ' رقم الهاتف مسجل بالفعل',
            'lat.required' => ' برجاء إدخال خط الطول',
            'lng.required' => ' برجاء إدخال خط العرض',
            'address.required' => ' برجاء إدخال العنوان',
        ];
    }
}
