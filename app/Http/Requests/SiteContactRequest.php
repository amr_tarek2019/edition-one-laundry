<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'title' => 'required|max:50',
            'details' => 'required|max:500',
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'برجاء إدخال العنوان',
            'details.required' => 'برجاء إدخال التفاصيل',
        ];
    }
}
