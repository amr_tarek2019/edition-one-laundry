<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteForgetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|max:50|exists:users,phone',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => 'برجاء إدخال رقم الهاتف',
            'phone.exists' => 'رقم الهاتف غير مسجل ',
        ];
    }
}
