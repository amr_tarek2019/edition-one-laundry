<?php

namespace App\Http\Controllers\Api;


use App\Http\Resources\Settings\SettingResource;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{

    public function index()
    {
        return apiResponse(200, 'إعدادات التطبيق', new SettingResource(Setting::first()), true);
    }

}
