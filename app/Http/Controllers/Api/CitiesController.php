<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Http\Controllers\Controller;
use App\Http\Resources\Cities\CitiesResourceCollection;
use Illuminate\Http\Request;


class CitiesController extends Controller
{

    public function index()
    {
        return apiResponse(200, 'المدن', new CitiesResourceCollection(City::all()));
    }

}
