<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Laundries\LaundriesResourceCollection;
use App\Laundry;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LaundryController extends Controller
{

    public function index(Request $request)
    {
        $per_page = $request->per_page ? $request->per_page : 5;
        $laundries = Laundry::where('is_active', 1)->where('expire_date', '>=', Carbon::today())
            ->when($request->has_offer, function ($laundry) {
                return $laundry->where('has_offers', 1);
            })->when($request->lat && $request->lng, function ($laundry) use ($request) {
                return $laundry->orderByRaw("( 6371 * acos ( cos ( radians(" . $request->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $request->lng . ") ) + sin ( radians(" . $request->lat . ") ) * sin( radians( lat ) ) ) <= 50) desc" );
            })
            ->get();
        return apiResponse(200, 'المغاسل', new LaundriesResourceCollection($laundries), true, $per_page);
    }

}
