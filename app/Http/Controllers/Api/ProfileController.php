<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\NotificationParserResourceCollection;
use App\Http\Resources\NotificationResourceCollection;
use App\Http\Resources\User\UserResource;
use App\PushNotification;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{

    public function updateInfo(Request $request)
    {
        $user = getUserByJwt($request);
        $user->update($request->all());
        return apiResponse(200,app()->isLocale('ar')  ? 'تم تعديل البيانات بنجاح' : 'updated successfully', new UserResource($user));
    }

    public function updatePassword(Request $request)
    {
        $v = validator($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|min:6|max:15',
            'new_password_confirmation' => 'required|same:new_password',
        ],
            [
                'old_password.required' => 'برجاء إدخال كلمه المرور القديمه',
                'new_password.required' => 'برجاء إدخال كلمه المرور الجديده',
                'new_password.min' => ' كلمه المرور الجديده لا تقل عن 6 حروف',
                'new_password.max' => ' كلمه المرور الجديده لا تزيد عن 15 حروف',
                'new_password_confirmation.required' => 'برجاء إدخال تأكيد كلمه المرور الجديده',
                'new_password_confirmation.same' => 'تأكيد كلمه المرور الجديده لا تتطابق',
            ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = getUserByJwt($request);
        $input = $request->all();
        if (Hash::check($request->old_password, $user->password)) {
            $user->update(['password' => $input['new_password']]);
            return apiResponse(200, app()->isLocale('ar')  ? 'تم تحديث كلمه المرور بنجاح' : 'updated successfully', new UserResource($user));
        }
        return apiResponse(205, app()->isLocale('ar')  ? 'كلمه المرور القديمه غير صحيحه' : 'updated successfully');

    }

    public function addAddress(Request $request)
    {
        $v = validator($request->all(), [
            'lat' => 'required|max:50',
            'lng' => 'required|max:50',
            'address' => 'required',

        ], [
            'lat.required' => ' برجاء إدخال خط الطول',
            'lng.required' => ' برجاء إدخال خط العرض',
            'address.required' => ' برجاء إدخال العنوان',

        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        $user = getUserByJwt($request);
        $user->addresses()->create([
            'lat' => $request->lat,
            'lng' => $request->lng,
            'address' => $request->address,
        ]);
        return apiResponse(200, app()->isLocale('ar')  ?'تم إضافه العنوان بنجاح' : 'added successfully', new UserResource($user));
    }

    public function updateNotificationToken(Request $request)
    {
        $v = validator($request->all(), [
            'notification_token' => 'required',
        ]);
        if ($v->fails()) {
            return apiResponse(405, $v->errors()->first());
        }
        getUserByJwt($request)->update(['notification_token' => $request->notification_token]);
        return apiResponse(200, 'تم التعديل بنجاح');
    }

    public function getNotifications(Request $request)
    {
        $user = getUserByJwt($request);
        $notifications = new NotificationParserResourceCollection($user->notifications);
        $push_notifications = new NotificationResourceCollection(PushNotification::where('type', 'all')->orWhere('type', 'users')->get());
        $merged_notification = collect($push_notifications)->merge(collect($notifications));
        return apiResponse(200, 'الإشعارات', $merged_notification, true);
    }

}
