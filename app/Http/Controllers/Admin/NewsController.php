<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;

class NewsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_news');
        $this->middleware('permission:add_news', ['only' => ['add']]);
        $this->middleware('permission:edit_news', ['only' => ['edit']]);
        $this->middleware('permission:delete_news', ['only' => ['delete']]);
    }

    public function index()
    {
        $rows = News::all();
        return view('admin.pages.news.index', ['rows' => $rows]);
    }



    public function delete($id)
    {
        $row = News::findOrFail($id);
        $destination = public_path('uploads/News');
        delete_file($row, 'img', $destination);
        $row->delete();
        return 1;
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:191',
            'img' => 'required|max:191',
            'description' => 'required',
        ]);
        News::create($request->all());
        return back()->withSuccess('تم الإضافه بنجاح');
    }

    public function edit(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|max:191',
            'description' => 'required',
        ]);
        News::findOrFail($request->id)->update($request->all());
        return back()->withSuccess('تم التعديل بنجاح');
    }
}
