<?php

namespace App\Http\Controllers\Admin;

use App\Delegate;
use App\PushNotification;
use App\traits\PushNotificationTrait;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    use PushNotificationTrait;

    function __construct()
    {
        $this->middleware('permission:show_notification');
        $this->middleware('permission:add_notification', ['only' => ['sendNotification']]);
    }

    public function index()
    {
        $notifications = PushNotification::all();
        return view('admin.pages.notifications.index', ['notifications' => $notifications]);
    }

    public function sendNotification(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'type' => 'required',
        ], [
            'title.required' => 'برجاء إدخال العنوان',
            'body.required' => 'برجاء إدخال التفاصيل',
            'type.required' => 'برجاء إختيار المستقبل',
        ]);
        $notification = PushNotification::create([
            'title' => $request->title,
            'body' => $request->body,
            'type' => $request->type,
        ]);
        if ($notification) {
            if ($request->type == 'all') {
                $users = User::whereNotNull('notification_token')->pluck('notification_token')->merge(Delegate::whereNotNull('notification_token')->pluck('notification_token'))->toArray();
            } elseif ($request->type == 'delegates') {
                $users = Delegate::whereNotNull('notification_token')->pluck('notification_token')->toArray();
            } elseif ($request->type == 'users') {
                $users = User::whereNotNull('notification_token')->pluck('notification_token')->toArray();
            }
            $chunked = array_chunk($users, 900);
            foreach ($chunked as $patch) {
                $this->PushNotification($request->title, $request->body, $patch, 'admin_notification');
            }
        }
        return back()->with('success', 'تم اللإرسال بنجاح');
    }
}
