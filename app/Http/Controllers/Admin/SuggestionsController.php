<?php

namespace App\Http\Controllers\Admin;

use App\Suggestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuggestionsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_suggestions');
        $this->middleware('permission:delete_suggestions', ['only' => ['deleteSuggestion']]);
    }
    public function index()
    {
        $suggestions = Suggestion::all();
        return view('admin.pages.suggestions.index', ['suggestions' => $suggestions]);
    }

    public function deleteSuggestion($id)
    {
        Suggestion::destroy($id);
        return 1;
    }
}
