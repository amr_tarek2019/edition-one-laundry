<?php

namespace App\Http\Controllers\Admin;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CitiesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:show_cities');
        $this->middleware('permission:add_cities', ['only' => ['add','add']]);
        $this->middleware('permission:edit_cities', ['only' => ['edit']]);
        $this->middleware('permission:delete_cities', ['only' => ['delete']]);
    }
    function index()
    {
        $cities = City::all();
        return view('admin.pages.cities.index', compact('cities'));
    }

    function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'name_en' => 'required|max:191',
            'delivery' => 'required|max:191',
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'name_en.required' => 'برجاء إدخال  الإسم',
            'delivery.required' => 'برجاء إدخال الشحن',
        ]);
        City::create($request->all());
        return back()->withSuccess('تم الإضافه بنجاح');
    }

    function edit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'name_en' => 'required|max:191',
            'delivery' => 'required|max:191',
        ], [
            'name.required' => 'برجاء إدخال  الإسم',
            'name_en.required' => 'برجاء إدخال  الإسم',
            'delivery.required' => 'برجاء إدخال الشحن',
        ]);
        City::findOrFail($request->id)->update($request->all());
        return back()->withSuccess('تم التعديل بنجاح');
    }

    public function delete($id)
    {
        City::findOrFail($id)->delete();
        return 1;
    }
}
