<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Laundry;
use Illuminate\Support\Carbon;
use App\Testimonial;
use App\News;
use App\Setting;
use App\Http\Requests\SiteContactRequest;
use App\Http\Requests\SiteEditProfile;
use App\User;
use App\Order;
use App\City;
use App\LaundryServices;
use App\Address;
use function GuzzleHttp\Psr7\readline;
use App\Admin;
use App\Delegate;
use App\Notifications\newOrderNotification;
use App\Events\newOrderEvent;
use App\traits\PushNotificationTrait;

class HomeController extends Controller
{
    use PushNotificationTrait;
    public function index()
    {
        $laundries = Laundry::where('is_active', 1)->where('expire_date', '>=', Carbon::today())->get();
        $testimorials = Testimonial::all();
        $news = News::latest()->take(2)->get();
        return view('site.pages.home', compact('laundries', 'testimorials', 'news'));
    }

    public function about()
    {
        $about = Setting::select('about')->first();
        return view('site.pages.about', compact('about'));
    }

    public function getContact()
    {
        return view('site.pages.contact');
    }
    public function postContact(SiteContactRequest $request)
    {
        auth('web')->user()->suggestions()->create($request->only(['title', 'details']));
        return back()->withSuccess('تم الارسال بنجاح');
    }

    public function getNews()
    {
        return view('site.pages.news', ['news' => News::all()]);
    }
    public function getNewsDetails(News $news)
    {
        return view('site.pages.news-details', compact('news'));
    }

    public function getFaq()
    {
        return view('site.pages.faq');
    }

    public function getProfile()
    {
        return view('site.pages.profile', ['user' => auth('web')->user()]);
    }

    public function postProfile(User $user, SiteEditProfile $request)
    {
        $user->update(array_filter($request->all(), function ($r) {
            return !is_null($r);
        }));
        return back()->withSuccess('تم بنجاح');
    }
    public function getTerms()
    {
        return view('site.pages.terms', ['terms' => Setting::select('terms')->first()]);
    }
    public function getCheckout(Request $request)
    {
        if (session()->has('cart')) {
            session()->forget('cart');
        }
        if (session()->has('total')) {
            session()->forget('total');
        }
        session()->put(['cart' => $request->cart, 'total' => $request->total]);
        return ['status' => 1];
    }
    public function getCheckoutConfirmation()
    {
        $user = auth('web')->user();
        $cities = City::all();
        return view('site.pages.checkout', compact('user', 'cities'));
    }
    public function postCheckout(Request $request)
    {
        $cart = session()->get('cart');
        if (!$cart) {
            return redirect()->route('site.home.index')->withErrors('Something Went Wrong , Please Try Again');
        }
        $service_ids = [];
        $service_price = [];
        $service_count = [];
        $service_type = [];
        $service_name = [];
        $service_name_en = [];
        foreach ($cart as $key => $value) {
            $service = LaundryServices::findOrFail($value['service_id']);
            array_push($service_ids, $service->id);
            array_push($service_name, $service->name);
            array_push($service_name_en, $service->name_en);
            array_push($service_type, $value['service_type']);
            array_push($service_count, $value['service_quantity']);
            array_push($service_price, $service[$value['service_type']]);
        }
        $address = Address::findOrFail($request->address);
        $all_services = LaundryServices::find($service_ids);
        $city = City::findOrFail($request->city);
        $order = Order::create([
            'services_id' => $service_ids,
            'services_type' => $service_type,
            'services_name' => $service_name,
            'services_name_en' => $service_name_en,
            'services_count' => $service_count,
            'services_price' => $service_price,
            'payment_type' => $request->payment_type,
            'user_id' => auth('web')->id(),
            'city' => $city->name,
            'laundry_id' => $all_services->first()->laundry->id,
            'delivery' => $city->delivery,
            'address' => $address->address,
            'lat' => $address->lat,
            'lng' => $address->lng,
        ]);
        if ($order) {
            session()->forget('cart');
            $admins = Admin::all()->filter(function ($admin) {
                return $admin->hasPermissionTo('show_orders');
            });
            $delegates = Delegate::whereNotNull('notification_token')->get();
            \Notification::send($admins, new newOrderNotification('طلب جديد', $order->id));

            $order->laundry->owner->notify(new newOrderNotification('طلب جديد', $order->id));

            \Notification::send($delegates, new newOrderNotification('طلب جديد', $order->id));

            $this->PushNotification('طلب جديد', 'يوجد طلب جديد', $delegates->pluck('notification_token')->toArray(), 'new_order', $order->id);
            event(new newOrderEvent('طلب جديد', $order->id));
            return redirect()->route('site.home.index')->withSuccess('تم تسجيل الطلب');
        }
    }

    public function myOrders()
    {
        return view('site.pages.my-orders', ['user' => auth('web')->user()]);
    }
    public function orderDetails(Order $order)
    {
        if (!$order || $order->user_id != auth('web')->id()) {
            return back()->withErrors('Error');
        }
        return view('site.pages.order-details', compact('order'));
    }
}
