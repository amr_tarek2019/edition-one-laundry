<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SiteRegisterRequest;
use App\User;
use App\Http\Requests\SiteCodeConfirmationRequest;
use App\Http\Requests\SiteForgetPasswordRequest;
use App\Http\Requests\SiteChangePasswordRequest;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\SiteLoginRequest;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function index()
    {
        return view('site.pages.auth.login');
    }
    public function getregister()
    {
        return view('site.pages.auth.register');
    }
    public function postRegister(SiteRegisterRequest $request)
    {
        $user = User::create($request->all());
        $user->addresses()->create($request->all());
        $this->sendCode($user, true);
        return redirect()->route('site.auth.code.get', ['phone' => $user->phone, 'forget' => false]);
    }

    public function getCode(Request $request)
    {
        if (!$request->phone || !User::wherePhone($request->phone)->first()) {
            return redirect()->route('site.auth.index')->withErrors('حدث خطأ , برجاء المحاوله مره اخرى ');
        }
        return view('site.pages.auth.code', ['phone' => $request->phone, 'forget' => $request->forget]);
    }

    public function postCode(SiteCodeConfirmationRequest $request)
    {
        $user = User::wherePhone($request->phone)->first();
        if (!$request->phone || !$user || !$user->code) {
            return redirect()->route('site.auth.index')->withErrors('حدث خطأ , برجاء المحاوله مره اخرى ');
        }
        $code = join("", array_reverse($request->code));
        if ($user->code->code === $code) {
            $this->sendCode($user);
            if ($request->forget) {
                return $this->getPassword($request->phone);
            } else {
                $user->update(['activated' => 1]);
                auth('web')->login($user);
                return redirect()->route('site.home.index');
            }
        }
        return back()->withErrors('كود غير صحيح');
    }

    public function getForget()
    {
        return view('site.pages.auth.forget');
    }

    public function postForget(SiteForgetPasswordRequest $request)
    {
        $user = User::wherePhone($request->phone)->first();
        $this->sendCode($user, true);
        return redirect()->route('site.auth.code.get', ['phone' => $user->phone, 'forget' => true]);
    }
    public function getPassword($phone)
    {
        return view('site.pages.auth.password', ['phone' => $phone]);
    }
    public function postPassword(Request $request)
    {
        $user = User::wherePhone($request->phone)->first();
        if (!$request->phone || !$user || !$user->code) {
            return redirect()->route('site.auth.index')->withErrors('حدث خطأ , برجاء المحاوله مره اخرى ');
        }
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6'
        ], [
            'password.required' => 'برجاء إدخال كلمه المرور',
            'password.confirmed' => 'برجاء التاكد من  تاكيد كلمه المرور ',
            'password.min' => 'برجاء إدخال ٦ أحرف على الاقل '
        ]);

        if ($validator->fails()) {
            return $this->getPassword($request->phone)->withErrors($validator->errors());
        }

        $user->update(['password' => $request->password]);
        $this->sendCode($user);
        auth('web')->login($user);
        return redirect()->route('site.home.index');
    }


    public function login(SiteLoginRequest $request)
    {
        $attempt = auth('web')->attempt($request->only(['phone', 'password']));
        if (!$attempt) {
            return back()->withErrors('برجاء التاكد من رقم الهاتف وكلمه المرور');
        }
        $user = auth('web')->user();
        if (!$user->activated) {
            $phone = $user->phone;
            auth('web')->logout();
            return redirect()->route('site.auth.code.get', ['phone' => $phone]);
        }
        return redirect()->intended();
    }

    public function logout()
    {
        auth('web')->logout();
        return redirect()->intended();
    }

    public function resendCode(Request $request)
    {
        $user = User::wherePhone($request->phone)->first();
        if (!$request->phone || !$user || !$user->code) {
            return redirect()->route('site.auth.index')->withErrors('حدث خطأ , برجاء المحاوله مره اخرى ');
        }
        if ($user->code && $user->code->updated_at->diffInMinutes(Carbon::now()) < 1) {
            return redirect()->route('site.auth.code.get', ['phone' => $user->phone])->withErrors('برجاء الإنتظار دقيقه قبل إعاده الإرسال');
        }
        $this->sendCode($user, true);
        return redirect()->route('site.auth.code.get', ['phone' => $user->phone])->withSuccess('تم الإرسال بنجاح');
    }

    public function sendCode(User $user, $send = false)
    {
        $code = generateRandomCode(4, $user->id);
        $user->code ?  $user->code()->update(['code' => $code]) : $user->code()->create(['code' => $code]);
        if ($send) {
            // $message = "your confirmation code is $code";
            // $client = new Client(['base_uri' => 'https://www.hisms.ws/']);
            // $client->request('GET', "api.php?send_sms&username=966564444947&password=Aa@123456&numbers=966$user->phone x&sender=laundry Sta&message= $message");
        }

        return $code;
    }
}
