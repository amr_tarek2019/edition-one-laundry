<?php

namespace App\Http\Resources\Cities;

use App\City;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CitiesResourceCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $this->collection->transform(function (City $city) {
            return [
                'id'=>$city->id,
                //'name'=>app()->isLocale('ar') ?  $city->name : $city->name_en,
                'delivery'=>$city->delivery,
            ];
        });

        return $this->collection->toArray();
    }
}
