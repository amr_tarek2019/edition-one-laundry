<?php

namespace App\Http\Resources\Laundries;

use App\LaundryDoc;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LaundriesDocsResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (LaundryDoc $laundryDoc) {
            return [
                'id' => $laundryDoc->id,
                'doc' => $laundryDoc->doc,
            ];
        });

        return $this->collection->toArray();
    }
}
