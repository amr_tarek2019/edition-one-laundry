<?php

namespace App\Http\Resources\Laundries;

use App\Laundry;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LaundriesResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Laundry $laundry) {
            return [
                'id' => $laundry->id,
                'name' => app()->isLocale('ar') ? $laundry->name : $laundry->name_en,
                'section_id' => $laundry->section_id,
                'description' => app()->isLocale('ar') ? $laundry->description : $laundry->description_en,
                'img' => $laundry->img,
                'lat' => $laundry->lat,
                'lng' => $laundry->lng,
                'address' => app()->isLocale('ar') ? $laundry->address : $laundry->address_en,
                'has_offers' => $laundry->has_offers,
                'phone' => $laundry->phone,
                'email' => $laundry->email,
                'rate' => $laundry->totalRate(),
                'docs' => new LaundriesDocsResourceCollection($laundry->docs),
                'services' => new LaundriesServicesResourceCollection($laundry->services),
            ];
        });
        return $this->collection->toArray();
    }
}
