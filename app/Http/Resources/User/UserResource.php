<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'social_token' => $this->social_token,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'img' => $this->img ,
            'social_img' => $this->social_img,
            'jwt' => $this->jwt,
            'addresses' => new AddressResourceCollection($this->addresses),
        ];
    }
}
