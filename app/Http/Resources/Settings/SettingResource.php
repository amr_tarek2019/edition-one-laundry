<?php

namespace App\Http\Resources\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'terms' => app()->isLocale('ar') ?  $this->terms : $this->terms_en,
            'about' => app()->isLocale('ar') ?  $this->about : $this->about_en,
        ];
    }
}
