<?php

namespace App\Http\Resources\Delegate\User;

use Illuminate\Http\Resources\Json\JsonResource;

class DelegateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'img' => $this->img ,
               'description' => $this->description,
                'order_type' => $this->order_type,
//            'identity_doc' => $this->identity_doc ,
            'jwt' => $this->jwt,
//            'activated' => $this->activated,
        ];
    }
}
