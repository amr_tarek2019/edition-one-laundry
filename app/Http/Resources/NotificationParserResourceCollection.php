<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\ResourceCollection;

class NotificationParserResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function ($notification) {
            return [
                'id' => $notification->id,
                'title' => $notification->data['title'],
                'body' => $notification->data['message'],
                'created_at' => $notification->created_at->toDateString(),

            ];

        });

        return $this->collection->toArray();
    }
}
