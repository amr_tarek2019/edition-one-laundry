<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $casts = [
        'services_id' => 'array',
        'services_name' => 'array',
        'services_name_en' => 'array',
        'services_type' => 'array',
        'services_count' => 'array',
        'services_price' => 'array',
    ];
    protected $fillable = [

        'services_id',
        'laundry_id',
        'services_name',
        'services_name_en',
        'services_type',
        'services_count',
        'services_price',
        'delegate_id',
        'order_status',
        'city',
        'delivery',
        'address',
        'user_id',
        'reason',
        'lat',
        'lng',
        'payment_type',
        'is_paid',
        'is_collected',
        'order_type',
        'description',
        'estimate_time'
    ];
    protected $appends = [
        'order_total'
    ];

    public function getDelegateIdAttribute($value)
    {
        return $value ? $value : 0;
    }
    
    

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }

    public function getReasonAttribute($value)
    {
        return $value ? $value : "";
    }
    
   

    public function getOrderTotalAttribute($value)
    {
        if($this->services_id!=0)
        {
        return array_sum($this->services_price) + intval($this->delivery);
        }else{
            return 0;
        }
            
        }

    public function getOrderServices()
    {
        $services = [];
        $count = count(array($this->services_id));
        

         if($this->services_id!=0){
         for ($i = 0 ; $i < $count ; $i++) {
            $service = LaundryServices::find($this->services_id[$i]);
            //dd($this->services_price[$i]);
            $data = [
                'service_id' => $this->services_id[$i],
                'service_name' => app()->isLocale('ar') ?  $this->services_name[$i] : $this->services_name_en[$i],
                'service_type' => $this->services_type[$i],
                'service_count' => $this->services_count[$i],
                'service_price' => $this->services_price[$i],
                'img' => $service ? $service->img : "",
                'service_total' => $this->services_price[$i] * $this->services_count[$i],
            ];
            array_push($services, $data);
         }
             
         }else{
             return [];
         }
        return $services;
         
    }

    public function laundry()
    {
        return $this->belongsTo(Laundry::class, 'laundry_id');
    }


    public function service($id)
    {
        return LaundryServices::find($id);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function chat()
    {
        return $this->hasOne(Chat::class);
    }

    public function delegate()
    {
        return $this->belongsTo(Delegate::class)->withDefault();
    }

    public function totalPrice()
    {
        //return array_sum($this->services_price) + intval($this->delivery);
          if($this->services_id!=0)
        {
        return array_sum($this->services_price) + intval($this->delivery);
        }else{
            return 0;
        }
    }

    public function servicePrice()
    {
        return array_sum($this->services_price);
    }

    public function rate()
    {
        return $this->hasOne(Rate::class, 'order_id');
    }
    
    
    public function getEstimatedTimeAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }

}
