<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [

        'about',
        'about_en',
        'terms',
        'terms_en',

    ];
}
