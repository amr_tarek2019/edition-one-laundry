<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'img',
        'jwt',
        'notification_token',
        'device_id',
        'is_active',
        'activated',
        'social_token',
        'social_img',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function code()
    {
        return $this->morphOne(Code::class, 'receiver');
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class)->where('order_status','!=',6);
    }

    public function ownOrder($order_id)
    {
        if (in_array($order_id, $this->orders->pluck('id')->toArray())) {
            return true;
        }
        return false;
    }


    public function getImgAttribute($value)
    {
        return $value ? asset('uploads/' . class_basename($this) . '/' . $value) : "";
    }

    public function getSocialImgAttribute($value)
    {
        return $value ? $value : "";
    }

    public function getSocialTokenAttribute($value)
    {
        return $value ? $value : "";
    }

    public function setImgAttribute($value)
    {
        if (is_file($value)) {
            if ($value) {
                $name = rand(0000, 9999) . time() . '.' . $value->getClientOriginalExtension();
                $value->move(public_path('uploads/' . class_basename($this)), $name);
                $this->attributes['img'] = $name;
            }
        } else {
            $this->attributes['img'] = $value;
        }
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }


    public function suggestions()
    {
        return $this->morphMany(Suggestion::class, 'user');
    }

    public function orderRate($order_id)
    {
        $rate = Rate::where('user_id', $this->id)->where('order_id', $order_id)->first();
        return $rate ? $rate->rate : -1;
    }
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'phone';
    }
}
