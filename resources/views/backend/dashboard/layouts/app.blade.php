<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Dashboard | Zircos - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Responsive bootstrap 4 admin template" name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('assets/dashboard/assets/images/favicon.ico') }}">

    <!-- App css -->
    <link href="{{asset('assets/dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
    <link href="{{asset('assets/dashboard/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/dashboard/assets/css/app-rtl.min.css') }}" rel="stylesheet" type="text/css" id="app-stylesheet" />
</head>

<body>

<!-- Begin page -->
<div id="wrapper">


    <!-- Topbar Start -->
@include('backend.dashboard.layouts.navbar')
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
@include('backend.dashboard.layouts.sidebar')
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
      @yield('content')
        <!-- end content -->



        <!-- Footer Start -->
    @include('backend.dashboard.layouts.footer')
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->

</div>
<!-- END wrapper -->



<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="{{asset('assets/dashboard/assets/js/vendor.min.js') }}"></script>

<script src="{{asset('assets/dashboard/assets/libs/morris-js/morris.min.js') }}"></script>
<script src="{{asset('assets/dashboard/assets/libs/raphael/raphael.min.js') }}"></script>

<script src="{{asset('assets/dashboard/assets/js/pages/dashboard.init.js') }}"></script>

<!-- App js -->
<script src="{{asset('assets/dashboard/assets/js/app.min.js') }}"></script>

</body>

</html>
