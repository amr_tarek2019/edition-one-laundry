<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Login | Zircos - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Responsive bootstrap 4 admin template" name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('assets/dashboard/assets/images/favicon.ico') }}">

    <!-- App css -->
    <link href="{{asset('assets/dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
    <link href="{{asset('assets/dashboard/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/dashboard/assets/css/app.min.css') }}" rel="stylesheet" type="text/css" id="app-stylesheet" />

</head>

<body>

<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card" style="margin-top: 100px;">

                    <div class="text-center account-logo-box">
                        <div class="mt-2 mb-2">
                            <a href="index.html" class="text-success">
                                <span><img src="{{asset('assets/dashboard/assets/images/logo1.png') }}" alt="" height="50"></span>
                            </a>
                        </div>
                    </div>

                    <div class="card-body">

                        <form action="#">

                            <div class="form-group">
                                <input class="form-control" type="text" id="username" required="" placeholder="Username">
                            </div>

                            <div class="form-group">
                                <input class="form-control" type="password" required="" id="password" placeholder="Password">
                            </div>

                            <div class="form-group account-btn text-center mt-2">
                                <div class="col-12">
                                    <button class="btn width-md btn-bordered btn-danger waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end card-body -->

                </div>
                <!-- end card -->
                <div class="mg-t-60 tx-center">
                    <a target="_blank" href="http://2grand.net/"> <img src="https://laundrystation.my-staff.net/android.png" width="100px" class="grand"
                                                                       style="
    margin-top: 40px;
    margin-left: 175px;
"> </a>
                </div>

            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->

</div>
<!-- end page -->

<!-- Vendor js -->
<script src="{{asset('assets/dashboard/assets/js/vendor.min.js') }}"></script>

<!-- App js -->
<script src="{{asset('assets/dashboard/assets/js/app.min.js') }}"></script>

</body>

</html>
