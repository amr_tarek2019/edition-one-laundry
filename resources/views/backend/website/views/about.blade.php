@extends('backend.website.layouts.app')
@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('assets/website/assets/images/about/page-title.png')}});">
    <div class="container">
        <div class="content-box">
            <div class="title"><h1>عن لاندرى ستيشن</h1></div>
            <ul class="bread-crumb rtl">
                <li><a href="index.html">الرئيسية</a></li>
                <li>عن لاندرى ستيشن</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- service details -->
<section class="service-details">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 service-column">
                <div class="service-details-content">
                    <div class="row">

                        <div class="col-md- col-sm- col-xs-12">
                            <div class="img-box"><figure><img src="{{asset('assets/website/assets/images/service/d33.jpg')}}" alt=""></figure></div>
                        </div>
                    </div>
                    <div class="content-style-one rtl">
                        <div class="title">عن لاندرى ستيشن</div>
                        <div class="text">
                            <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال "lorem ipsum" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها
                                م استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال "lorem ipsum" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها
                                م استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال "lorem ipsum" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها
                                م استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال "lorem ipsum" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها

                                .</p>
                        </div>
                    </div>
                    <!--
                                            <div class="content-style-two rtl">
                                                <h3> Tincidunt Ante Enim</h3>
                                                <p>Excepteur sint occaecat cupidatat non proident sunt culpa qui officia deserunt mollit anim est labor. Sed perspiciatis unde omnis iste natus error.sit voluptatem accusantium doloremque laudantium totam rem aperiam eaque ipsa quae ab illo inventore verit atis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                                <div class="button"><a href="#" class="btn-one">Calculate Service</a></div>
                                            </div>
                    -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- service details end -->
<section class="we-work" style="background-image: url({{asset('assets/website/assets/images/home/work.png')}});">
    <div class="container">
        <div class="title-top centred">الخدمات</div>
        <div class="sec-title centred"><h2>تمتع بمزايا عديدة فى خدماتنا</h2></div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 column">
                <div class="single-item">
                    <div class="icon-box">
                        <i>
                            <img src="{{asset('assets/website/assets/images/washing-machine.png')}}" class="wash-icon">
                        </i>

                        <div class="number">1</div>
                    </div>
                    <div class="text">غسيل</div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 column">
                <div class="single-item">
                    <div class="icon-box">
                        <i>
                            <img src="{{asset('assets/website/assets/images/iron.png')}}" class="wash-icon">
                        </i>
                        <div class="number">2</div>
                    </div>
                    <div class="text">كي</div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 column">
                <div class="single-item">
                    <div class="icon-box">
                        <i>
                            <img src="{{asset('assets/website/assets/images/steam.png')}}" class="wash-icon">
                        </i>
                        <div class="number">3</div>
                    </div>
                    <div class="text">غسيل و كي بالبخار</div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 column">
                <div class="single-item">
                    <div class="icon-box">
                        <i>
                            <img src="{{asset('assets/website/assets/images/logistics.png')}}" class="wash-icon">
                        </i>
                        <div class="number">4</div>
                    </div>
                    <div class="text">توصيل للمنازل</div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-section" >
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 about-column">
                <div class="img-box wow slideInLeft animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInLeft;"><figure><img src="{{asset('assets/website/assets/images/about/1.jpg') }}" alt=""></figure></div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 about-column">
                <div class="about-content text-right">
                    <div class="title-top">مرحبا بك </div>
                    <div class="sec-title"><h2>موقع و تطبيق لاندرى ستيشن</h2></div>
                    <div class="text">
                        <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء
                            هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء</p>
                    </div>
                    <div class="button"><a href="contact.html" class="btn-one">أطلب خدمتك الأن</a></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
