@extends('backend.website.layouts.app')
@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('assets/website/assets/images/about/page-title3.png)')}}">
    <div class="container">
        <div class="content-box">
            <div class="title"><h1>مغسلة الإخلاص و الأمل</h1></div>
            <ul class="bread-crumb rtl">
                <li><a href="index.html">الرئيسية</a></li>
                <li>مغسلة الإخلاص و الأمل</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->

<section class="team-details about-section rtl" style="padding-bottom: 10px">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8 col-xs-12 column">
                <div class="img-box">
                    <figure><img src="{{asset('assets/website/assets/images/gallery/5.jpg')}}" alt=""></figure>
                </div>
            </div>
            <div class="col-md-7 col-sm-8 col-xs-12 column">
                <div class="team-details-content">
                    <div class="content-style-one">
                        <div class="title">مغسلة الإخلاص و الأمل</div>
                        <div class="text primary text-bold" >خصم</div>
                        <!-- Button trigger modal -->


                        <div class="text">
                            <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم
                            </p>
                        </div>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                            موقعنا على الخريطة
                        </button>
                        <ul class="contact-info">
                            <li><i class="fa fa-phone"><a href="#">(+1) 251-235-3256</a></i></li>
                            <li><i class="fa fa-envelope"></i><a href="#">info@wilfordwood.com</a></li>
                        </ul>
                        <ul class="team-social">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- single shop -->

<section class="service-section" style="padding-top: 50px">
    <div class="container">
        <div class="service-title centred">
            <div class="title-top">منتجاتنا  </div>
            <div class="sec-title"><h2>تمتع بكل المزايا و الخدمات معنا</h2></div>
        </div>
        <ul class="post-filter list-inline centred rtl">
            <li class="active" data-filter=".filter-item">
                <span>جميع الخدمات</span>
            </li>
            <li data-filter=".Consulting">
                <span>غسيل</span>
            </li>
            <li data-filter=".Finance">
                <span>كي</span>
            </li>

        </ul>

        <div class="row masonary-layout filter-layout">
            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Consulting ">
                <article class="single-column single-column2">
                    <div class="item rtl">
                        <figure class="img-box">
                            <img src="{{asset('assets/website/assets/images/shop/1.jpg') }}" alt="">
                            <figcaption class="default-overlay-outer">
                                <div class="inner">
                                    <div class="content-layer">
                                        <a href="{{asset('assets/website/assets/images/shop/1.jpg') }}" class="btn-two lightbox-image">عرض سريع</a>
                                        <a href="#" class="btn-one">اضف الى قائمة الطلبات</a>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="lower-content">
                            <ul class="counter">
                                <li>  <h5><a href="#">بدلة رجالى</a></h5>  </li>
                                <li>
                                    <div class="qty mt-5 ltr">
                                        <span class="minus bg-dark">-</span>
                                        <input type="number" class="count" name="qty" value="0" disabled="">
                                        <span class="plus bg-dark">+</span>
                                    </div>  </li>


                            </ul>


                            <div class="price">السعر <span>: 50 ريال سعودى</span></div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Consulting">
                <article class="single-column single-column2">
                    <div class="item rtl">
                        <figure class="img-box">
                            <img src="{{asset('assets/website/assets/images/shop/10.jpg') }}" alt="">
                            <figcaption class="default-overlay-outer">
                                <div class="inner">
                                    <div class="content-layer">
                                        <a href="{{asset('assets/website/assets/images/shop/10.jpg') }}" class="btn-two lightbox-image">عرض سريع</a>
                                        <a href="#" class="btn-one">اضف الى قائمة الطلبات</a>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="lower-content">
                            <ul class="counter">
                                <li>  <h5><a href="#">فستان حريمي </a></h5>  </li>
                                <li>
                                    <div class="qty mt-5 ltr">
                                        <span class="minus bg-dark">-</span>
                                        <input type="number" class="count" name="qty" value="0" disabled="">
                                        <span class="plus bg-dark">+</span>
                                    </div>  </li>


                            </ul>


                            <div class="price">السعر <span>: 50 ريال سعودى</span></div>
                        </div>
                    </div>
                </article>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Finance Finance">
                <article class="single-column single-column2">
                    <div class="item rtl">
                        <figure class="img-box">
                            <img src="{{asset('assets/website/assets/images/shop/11.jpg') }}" alt="">
                            <figcaption class="default-overlay-outer">
                                <div class="inner">
                                    <div class="content-layer">
                                        <a href="{{asset('assets/website/assets/images/shop/11.jpg') }}" class="btn-two lightbox-image">عرض سريع</a>
                                        <a href="#" class="btn-one">اضف الى قائمة الطلبات</a>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="lower-content">
                            <ul class="counter">
                                <li>  <h5><a href="#">قميص رجالى</a></h5>  </li>
                                <li>
                                    <div class="qty mt-5 ltr">
                                        <span class="minus bg-dark">-</span>
                                        <input type="number" class="count" name="qty" value="0" disabled="">
                                        <span class="plus bg-dark">+</span>
                                    </div>  </li>


                            </ul>


                            <div class="price">السعر <span>: 50 ريال سعودى</span></div>
                        </div>
                    </div>
                </article>
            </div>

            <br/>

            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Finance Finance">
                <article class="single-column single-column2">
                    <div class="item rtl">
                        <figure class="img-box">
                            <img src="{{asset('assets/website/assets/images/shop/12.jpg') }}" alt="">
                            <figcaption class="default-overlay-outer">
                                <div class="inner">
                                    <div class="content-layer">
                                        <a href="{{asset('assets/website/assets/images/shop/12.jpg') }}" class="btn-two lightbox-image">عرض سريع</a>
                                        <a href="#" class="btn-one">اضف الى قائمة الطلبات</a>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="lower-content">
                            <ul class="counter">
                                <li>  <h5><a href="#">مناشف قطنية</a></h5>  </li>
                                <li>
                                    <div class="qty mt-5 ltr">
                                        <span class="minus bg-dark">-</span>
                                        <input type="number" class="count" name="qty" value="0" disabled="">
                                        <span class="plus bg-dark">+</span>
                                    </div>  </li>


                            </ul>


                            <div class="price">السعر <span>: 50 ريال سعودى</span></div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Consulting Finance">
                <article class="single-column single-column2">
                    <div class="item rtl">
                        <figure class="img-box">
                            <img src="{{asset('assets/website/assets/images/shop/13.jpg') }}" alt="">
                            <figcaption class="default-overlay-outer">
                                <div class="inner">
                                    <div class="content-layer">
                                        <a href="{{asset('assets/website/assets/images/shop/13.jpg') }}" class="btn-two lightbox-image">عرض سريع</a>
                                        <a href="#" class="btn-one">اضف الى قائمة الطلبات</a>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="lower-content">
                            <ul class="counter">
                                <li>  <h5><a href="#">قمصان قطنية</a></h5>  </li>
                                <li>
                                    <div class="qty mt-5 ltr">
                                        <span class="minus bg-dark">-</span>
                                        <input type="number" class="count" name="qty" value="0" disabled="">
                                        <span class="plus bg-dark">+</span>
                                    </div>  </li>


                            </ul>


                            <div class="price">السعر <span>: 50 ريال سعودى</span></div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Finance">
                <article class="single-column single-column2">
                    <div class="item rtl">
                        <figure class="img-box">
                            <img src="{{asset('assets/website/assets/images/shop/14.jpg') }}" alt="">
                            <figcaption class="default-overlay-outer">
                                <div class="inner">
                                    <div class="content-layer">
                                        <a href="{{asset('assets/website/assets/images/shop/14.jpg') }}" class="btn-two lightbox-image">عرض سريع</a>
                                        <a href="#" class="btn-one">اضف الى قائمة الطلبات</a>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                        <div class="lower-content">
                            <ul class="counter">
                                <li>  <h5><a href="#">تيشرت قطني</a></h5>  </li>
                                <li>
                                    <div class="qty mt-5 ltr">
                                        <span class="minus bg-dark">-</span>
                                        <input type="number" class="count" name="qty" value="0" disabled="">
                                        <span class="plus bg-dark">+</span>
                                    </div>  </li>


                            </ul>


                            <div class="price">السعر <span>: 50 ريال سعودى</span></div>
                        </div>
                    </div>
                </article>
            </div>

        </div>

        <a href="adress.html" class="btn-one btn-one center" >
            الإجمالى <span>300 ريال سعودي</span>
        </a>

    </div>
</section>
@endsection
