@extends('backend.website.layouts.app')
@section('content')
    <!-- page title -->
    <section class="page-title centred" style="background-image: url({{asset('assets/website/assets/images/about/page-title.png')}});">
        <div class="container">
            <div class="content-box rtl">
                <div class="title"><h1>اتصل بنا</h1></div>
                <ul class="bread-crumb">
                    <li><a href="index.html">الرئيسية</a></li>
                    <li>إتصل بنا</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!-- contact section -->
    <section class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 contact-column">
                    <div class="google-map-area">
                        <div
                            class="google-map"
                            id="contact-google-map"
                            data-map-lat="40.086097"
                            data-map-lng="-105.939460"
                            data-icon-path="images/resources/map-marker.png"
                            data-map-title="saudi arabia, New York, United Kingdom"
                            data-map-zoom="12"
                            data-markers='{
                                "marker-1": [40.086097, -105.939460, "<h4>Branch Office</h4><p>77/99 London UK</p>","assets/website/assets/images/resources/map-marker.png"]
                            }'>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 contact-column">
                    <div class="contact-info">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="single-info">
                                    <div class="icon-box"><i class="flaticon-gps"></i></div>
                                    <h4>الموقع</h4>
                                    <div class="text">المملكة العربية السعودية - الخبر </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="single-info">
                                    <div class="icon-box"><i class="flaticon-technology"></i></div>
                                    <h4>الهاتف</h4>

                                    <div class="cart-box ">
                                        <a href="https://wa.me/15551234567" target="_blank"> <i class="fab fa-whatsapp" aria-hidden="true">
                                            </i> (+020) 564-334-21-22-34 </a>
                                        <br>
                                        <a href="https://wa.me/15551234567" target="_blank"> <i class="fab fa-whatsapp" aria-hidden="true">
                                            </i> (+48) 564-334-21-22-38 </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="single-info">
                                    <div class="icon-box"><i class="flaticon-message"></i></div>
                                    <h4>البريد الالكتروني</h4>
                                    <div class="text">info@templatepath.com <br />info@sharpes.com</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="single-info">
                                    <div class="icon-box"><i class="flaticon-globe"></i></div>
                                    <h4>الموقع الالكتروني</h4>
                                    <div class="text">www.laundrystation.my-staff.net/</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-form-area">
            <div class="container">
                <div class="title-top centred">إرسل رسالتك</div>
                <div class="sec-title centred"><h2>كن على تواصل معنا دائما</h2></div>
                <form id="contact-form" name="contact_form" class="default-form" action="http://azim.commonsupport.com/Sharpes/inc/sendmail.php" method="post">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="form_name" value="" placeholder="الاسم" required="">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" name="form_email" value="" placeholder="البريد" required="">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="form_phone" value="" placeholder="الهاتف" required="">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="form_subject" value="" placeholder='عنوان الرسالة' required="">
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea placeholder="الرسالة" name="form_message" required=""></textarea>
                        </div>
                    </div>
                    <div class="contact-btn centred"><button type="submit" class="btn-one" data-loading-text="Please wait...">إرسال </button></div>
                </form>
            </div>
        </div>
    </section>
    <!-- contact section end -->



@endsection
