@extends('backend.website.layouts.app')
@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('assets/website/assets/images/about/page-title.png')}});">
    <div class="container">
        <div class="content-box">
            <div class="title"><h1>جميع مغاسلنا</h1></div>
            <ul class="bread-crumb rtl">
                <li><a href="index.html">الرئيسية</a></li>
                <li>المغاسل</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- service section -->
<section class="service-section sec-pad">
    <div class="container">
        <div class="service-title centred">
            <div class="title-top">جميع المغاسل</div>
            <div class="sec-title"><h2>دائما نكون فى خدمتك</h2></div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/1.jpg')}}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="dry-clean-details.html"><i class="fa fa-link"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="dry-clean-details.html">مغسلة الأمانة</a></h3>
                        <li>على بعد  <span class="primary-color">10 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>

                    </div>

                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/2.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{asset('assets/website/assets/images/gallery/2.jpg') }}" class="lightbox-image"><i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="dry-clean-details.html">مغسلة الأمانة</a></h3>
                        <li>على بعد  <span class="primary-color">7 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/3.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{asset('assets/website/assets/images/gallery/3.jpg') }}" class="lightbox-image"><i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="dry-clean-details.html">مغسلة الأمانة</a> <span class="offers"><i class="fas fa-badge-percent"></i>
                                خصم
                                50%</span></h3>
                        <li>على بعد  <span class="primary-color">20 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/4.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{asset('assets/website/assets/images/gallery/4.jpg') }}" class="lightbox-image"><i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="dry-clean-details.html">مغسلة الأمانة</a><i class="fa fa-off"></i></h3>
                        <li>على بعد  <span class="primary-color">22 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/6.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{asset('assets/website/assets/images/gallery/6.jpg') }}" class="lightbox-image"><i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="dry-clean-details.html">مغسلة يونيفيرسال</a> <span class="offers"><i class="fas fa-badge-percent"></i>
                                خصم
                                50%</span></h3>
                        <li>على بعد  <span class="primary-color">15 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/5.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{asset('assets/website/assets/images/gallery/5.jpg') }}" class="lightbox-image"><i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="dry-clean-details.html">مغسلة الأمانة</a></h3>
                        <li>على بعد  <span class="primary-color">13 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- service section end -->
@endsection
