@extends('backend.website.layouts.app')
@section('content')
<section class="main-slider">
    <div class="container-fluid">
        <ul class="main-slider-carousel owl-carousel owl-theme slide-nav">
            <li class="slider-wrapper">
                <div class="image"><img src="{{asset('assets/website/assets/images/slider/1.png') }}" alt=""></div>
                <div class="slider-caption">
                    <div class="container">
                        <div class="tp-title">إجعل منزلك مضيئ</div>
                        <h1>لسنا الوحيدون <br />ولكننا الأفضل و الأسرع </h1>
                        <div class="tp-btn">
                            <a href="dry-clean.html" class="btn-one">أحجز الأن</a>
                            <a href="contact.html" class="btn-two">اتصل بنا</a>
                        </div>
                    </div>
                </div>
                <div class="slide-overlay"></div>
            </li>
            <li class="slider-wrapper">
                <div class="image"><img src="{{asset('assets/website/assets/images/slider/2.png') }}" alt=""></div>
                <div class="slider-caption">
                    <div class="container">
                        <div class="tp-title">إجعل منزلك مضيئ</div>
                        <h1>لسنا الوحيدون <br />ولكننا الأفضل و الأسرع </h1>
                        <div class="tp-btn">
                            <a href="dry-clean.html" class="btn-one">أحجز الأن</a>
                            <a href="contact.html" class="btn-two">اتصل بنا</a>
                        </div>
                    </div>
                </div>
                <div class="slide-overlay"></div>
            </li>
            <li class="slider-wrapper">
                <div class="image"><img src="{{asset('assets/website/assets/images/slider/3.png') }}" alt=""></div>
                <div class="slider-caption">
                    <div class="container">
                        <div class="tp-title">إجعل منزلك مضيئ</div>
                        <h1>لسنا الوحيدون <br />ولكننا الأفضل و الأسرع </h1>
                        <div class="tp-btn">
                            <a href="dry-clean.html" class="btn-one">أحجز الأن</a>
                            <a href="contact.html" class="btn-two">اتصل بنا</a>
                        </div>
                    </div>
                </div>
                <div class="slide-overlay"></div>
            </li>
        </ul>
    </div>
</section>
<!-- main-slider end -->


<!-- about section -->
<section class="about-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 about-column">
                <div class="img-box wow slideInLeft" data-wow-delay="0ms" data-wow-duration="1500ms"><figure><img src="{{asset('assets/website/assets/images/about/1.jpg') }}" alt=""></figure></div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 about-column">
                <div class="about-content text-right">
                    <div class="title-top">مرحبا بك </div>
                    <div class="sec-title"><h2>موقع و تطبيق لاندرى ستيشن</h2></div>
                    <div class="text">
                        <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء
                            هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء</p>
                    </div>
                    <div class="button"><a href="dry-clean.html" class="btn-one">أطلب خدمتك الأن</a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- about section end -->


<!-- cta section -->
<section class="cta-section" style="background-image: url({{asset('assets/website/assets/images/home/cta.png')}});">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12 cta-column">
                <div class="cta-content text-right">
                    <div class="title"><h1>احنا علينا التنضيف  <br /> و انت عليك ترتاح</h1></div>
                    <div class="text">
                        <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء
                            هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء</p>
                    </div>
                    <div class="video-area">
                        <div class="button"><a href="dry-clean-details.html" class="btn-one">تعرف على خدماتنا</a></div>
                        <div class="video-gallery">
                            <div class="icon-holder">
                                <div class="icon">
                                    <a class="html5lightbox" title="Cleaning Video" href="https://www.youtube.com/watch?v=sXoU5GbNvGs&list=PLd6F5L7gdibiGPZdhlmSOi-elnoPWst3Q"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- cta section end -->


<!-- service section -->
<!--<section class="service-section">
    <div class="container">
        <div class="service-title centred">
            <div class="title-top">Our Services</div>
            <div class="sec-title"><h2>What We Offer</h2></div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item text-right">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <a href="#"><img src="{{asset('assets/website/assets/images/service/1.') }}" alt=""></a>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="service-details.html"><i class="fa fa-link"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content">
                        <h3><a href="service-details.html">House Cleaning</a></h3>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item text-right ">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <a href="#"><img src="{{asset('assets/website/assets/images/service/2.jpg') }}" alt=""></a>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="service-details.html"><i class="fa fa-link"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content">
                        <h3><a href="service-details.html">Office Cleaning</a></h3>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item text-right">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <a href="#"><img src="{{asset('assets/website/assets/images/service/3.jpg') }}" alt=""></a>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="service-details.html"><i class="fa fa-link"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content">
                        <h3><a href="service-details.html">مغسلة الأمانة</a></h3>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item text-right">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <a href="#"><img src="{{asset('assets/website/assets/images/service/4.jpg') }}" alt=""></a>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="service-details.html"><i class="fa fa-link"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content">
                        <h3><a href="service-details.html">House Cleaning</a></h3>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item text-right">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <a href="#"><img src="{{asset('assets/website/assets/images/service/5.jpg') }}" alt=""></a>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="service-details.html"><i class="fa fa-link"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content">
                        <h3><a href="service-details.html">Kitchen Cleaning</a></h3>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item text-right">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <a href="#"><img src="{{asset('assets/website/assets/images/service/6.jpg') }}" alt=""></a>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="service-details.html"><i class="fa fa-link"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content">
                        <h3><a href="service-details.html">House Cleaning</a></h3>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- service section end -->
<section class="service-section">
    <div class="container">
        <div class="service-title centred">
            <div class="title-top">خدماتنا </div>
            <div class="sec-title"><h2>إليك أفضل المغاسل الموجودة</h2></div>
        </div>
        <ul class="post-filter list-inline centred rtl">
            <li class="active" data-filter=".filter-item">
                <span>الكل</span>
            </li>
            <li data-filter=".Consulting">
                <span>الأقرب</span>
            </li>
            <li data-filter=".Finance">
                <span>العروض</span>
            </li>

        </ul>

        <div class="row masonary-layout filter-layout">
            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Consulting ">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/1.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="sub-category.html"><i class="fa fa-link"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="sub-category.html">مغسلة الأمانة</a></h3>
                        <li>على بعد  <span class="primary-color">10 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>

                    </div>

                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Consulting">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/2.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{asset('assets/website/assets/images/gallery/2.jpg') }}" class="lightbox-image"><i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="sub-category.html">مغسلة الأمانة</a></h3>
                        <li>على بعد  <span class="primary-color">7 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Finance Finance">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/3.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{asset('assets/website/assets/images/gallery/3.jpg') }}" class="lightbox-image"><i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="sub-category.html">مغسلة الأمانة</a> <span class="offers"><i class="fas fa-badge-percent"></i>
                                خصم
                                50%</span></h3>
                        <li>على بعد  <span class="primary-color">20 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Finance Finance">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/4.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{asset('assets/website/assets/images/gallery/4.jpg') }}" class="lightbox-image"><i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="sub-category.html">مغسلة الأمانة</a><i class="fa fa-off"></i></h3>
                        <li>على بعد  <span class="primary-color">22 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Consulting Finance">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/5.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{asset('assets/website/assets/images/gallery/5.jpg') }}" class="lightbox-image"><i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="sub-category.html">مغسلة الأمانة</a></h3>
                        <li>على بعد  <span class="primary-color">13 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 filter-item Finance">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{asset('assets/website/assets/images/gallery/6.jpg') }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{asset('assets/website/assets/images/gallery/6.jpg') }}" class="lightbox-image"><i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="sub-category.html">مغسلة يونيفيرسال</a> <span class="offers"><i class="fas fa-badge-percent"></i>
                                خصم
                                50%</span></h3>
                        <li>على بعد  <span class="primary-color">15 كيلو متر</span></li>
                        <div class="text">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</div>
                    </div>
                </div>
            </div>

        </div>
        <a href="dry-clean.html" class="btn-one center">عرض جميع المغاسل</a>
    </div>
</section>

<!-- we-work -->
<section class="we-work" style="background-image: url({{asset('assets/website/assets/images/home/work.png')}});">
    <div class="container">
        <div class="title-top centred">الخدمات</div>
        <div class="sec-title centred"><h2>تمتع بمزايا عديدة فى خدماتنا</h2></div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 column">
                <div class="single-item">
                    <div class="icon-box">
                        <i >
                            <img src="{{asset('assets/website/assets/images/washing-machine.png') }}" class="wash-icon">
                        </i>

                        <div class="number">1</div>
                    </div>
                    <div class="text">غسيل</div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 column">
                <div class="single-item">
                    <div class="icon-box">
                        <i >
                            <img src="{{asset('assets/website/assets/images/iron.png') }}" class="wash-icon">
                        </i>
                        <div class="number">2</div>
                    </div>
                    <div class="text">كي</div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 column">
                <div class="single-item">
                    <div class="icon-box">
                        <i >
                            <img src="{{asset('assets/website/assets/images/steam.png') }}" class="wash-icon">
                        </i>
                        <div class="number">3</div>
                    </div>
                    <div class="text">غسيل و كي بالبخار</div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 column">
                <div class="single-item">
                    <div class="icon-box">
                        <i >
                            <img src="{{asset('assets/website/assets/images/logistics.png') }}" class="wash-icon">
                        </i>
                        <div class="number">4</div>
                    </div>
                    <div class="text">توصيل للمنازل</div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end we-work section -->


<!-- pricing-section -->
<section class="pricing-section centred sec-pad">
    <div class="container">
        <div class="title-top">أسعارنا</div>
        <div class="sec-title"><h2>خطط أسعار لاندرى ستيشن</h2></div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 pricing-column">
                <div class="single-table">
                    <div class="top-content">
                        <div class="title">اليومية</div>
                        <div class="text">10.00
                            <br>
                            <span>ريال / يوميا</span></div>
                    </div>
                    <div class="lower-content">
                        <ul class="list">
                            <li class="rtl" ><i class="fa fa-check-circle-o" aria-hidden="true"></i>5 تيشيرت</li>
                            <li class="rtl"><i class="fa fa-check-circle-o" aria-hidden="true"></i>3 بنطلون</li>
                            <li class="rtl"><i class="fa fa-check-circle-o" aria-hidden="true"></i>قفطان</li>
                            <li class="rtl"><i class="fa fa-check-circle-o" aria-hidden="true"></i>عباية</li>
                        </ul>
                        <div class="button"><a href="dry-clean-details.html" class="btn-two">اشترك الأن</a></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 pricing-column">
                <div class="single-table">
                    <div class="top-content">
                        <div class="title">الشهرية</div>
                        <div class="text">49.00
                            <br>
                            <span>ريال / شهريا</span></div>
                    </div>
                    <div class="lower-content">
                        <ul class="list">
                            <li class="rtl" ><i class="fa fa-check-circle-o" aria-hidden="true"></i>5 تيشيرت</li>
                            <li class="rtl"><i class="fa fa-check-circle-o" aria-hidden="true"></i>3 بنطلون</li>
                            <li class="rtl"><i class="fa fa-check-circle-o" aria-hidden="true"></i>قفطان</li>
                            <li class="rtl"><i class="fa fa-check-circle-o" aria-hidden="true"></i>عباية</li>
                        </ul>
                        <div class="button"><a href="dry-clean-details.html" class="btn-two">اشترك الأن</a></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 pricing-column">
                <div class="single-table">
                    <div class="top-content">
                        <div class="title">السنوية</div>
                        <div class="text">
                            79.00
                            <br>
                            <span>ريال / سنويا</span>
                        </div>
                    </div>
                    <div class="lower-content">
                        <ul class="list">
                            <li class="rtl" ><i class="fa fa-check-circle-o" aria-hidden="true"></i>5 تيشيرت</li>
                            <li class="rtl"><i class="fa fa-check-circle-o" aria-hidden="true"></i>3 بنطلون</li>
                            <li class="rtl"><i class="fa fa-check-circle-o" aria-hidden="true"></i>قفطان</li>
                            <li class="rtl"><i class="fa fa-check-circle-o" aria-hidden="true"></i>عباية</li>
                        </ul>
                        <div class="button"><a href="dry-clean-details.html" class="btn-two">اشترك الأن</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- pricing section end -->


<!-- testimonials section -->
<section class="testimonials-section centred" style="background-image: url({{asset('assets/website/assets/images/home/testimonial.png')}});">
    <div class="container">
        <div class="title-top">أراء عملائمنا</div>
        <div class="sec-title"><h2>ما يقوله عملائنا عنا </h2></div>
        <div class="row">
            <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 testimonial-column">
                <div class="client-testimonial-carousel owl-carousel owl-theme">
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">مغسلة رائعة و سعدت بالتعامل معهم . قمة فى الأدب و التعامل الراقى اود ان اشكر جميع القائمين على المشروع و على تنفيذ التطبيقات و الموقع الالكتروني</div>
                            <div class="author">يسرا محسن</div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">مغسلة رائعة و سعدت بالتعامل معهم . قمة فى الأدب و التعامل الراقى اود ان اشكر جميع القائمين على المشروع و على تنفيذ التطبيقات و الموقع الالكتروني</div>
                            <div class="author">أحمد بكري</div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">مغسلة رائعة و سعدت بالتعامل معهم . قمة فى الأدب و التعامل الراقى اود ان اشكر جميع القائمين على المشروع و على تنفيذ التطبيقات و الموقع الالكتروني</div>
                            <div class="author">منى الحاوى</div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">مغسلة رائعة و سعدت بالتعامل معهم . قمة فى الأدب و التعامل الراقى اود ان اشكر جميع القائمين على المشروع و على تنفيذ التطبيقات و الموقع الالكتروني</div>
                            <div class="author">امال ماجد</div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">مغسلة رائعة و سعدت بالتعامل معهم . قمة فى الأدب و التعامل الراقى اود ان اشكر جميع القائمين على المشروع و على تنفيذ التطبيقات و الموقع الالكتروني</div>
                            <div class="author">أسامة محسن</div>
                        </div>
                    </div>
                    <div class="testimonial-content">
                        <div class="inner-box">
                            <div class="text">مغسلة رائعة و سعدت بالتعامل معهم . قمة فى الأدب و التعامل الراقى اود ان اشكر جميع القائمين على المشروع و على تنفيذ التطبيقات و الموقع الالكتروني</div>
                            <div class="author">ابتسام صلاح</div>
                        </div>
                    </div>

                </div>

                <!--Client Thumbs Carousel-->
                <div class="client-thumb-outer">
                    <div class="client-thumbs-carousel owl-carousel owl-theme">
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{asset('assets/website/assets/images/testimonial/1.png') }}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{asset('assets/website/assets/images/testimonial/2.png') }}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{asset('assets/website/assets/images/testimonial/3.png') }}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{asset('assets/website/assets/images/testimonial/1.png') }}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{asset('assets/website/assets/images/testimonial/2.png') }}" alt=""></figure>
                        </div>
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{asset('assets/website/assets/images/testimonial/3.png') }}" alt=""></figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- testimonial section end -->


<!-- news section -->
<section class="news-section">
    <div class="container">
        <div class="news-title centred">
            <div class="title-top">أخبار المغسلة</div>
            <div class="sec-title"><h2>أخر الأخبار عن لاندرى ستيشن</h2></div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 news-column">
                <div class="single-item text-right">
                    <div class="img-box"><a href="news-details.html"><figure><img src="{{asset('assets/website/assets/images/news/1.jpg') }}" alt=""></figure></a></div>
                    <div class="lower-content">
                        <h3><a href="news-details.html">قريبا اضافة خدمات التنظيف المنزلى</a></h3>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 news-column">
                <div class="single-item text-right">
                    <div class="img-box"><a href="news-details.html"><figure><img src="{{asset('assets/website/assets/images/news/2.jpg') }}" alt=""></figure></a></div>
                    <div class="lower-content">
                        <h3><a href="news-details.html">قريبا اضافة خدمات التنظيف المنزلى</a></h3>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- news section end -->
@endsection
