@extends('backend.website.layouts.app')
@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('assets/website/assets/images/about/page-title.png')}});">
    <div class="container">
        <div class="content-box">
            <div class="title"><h1>طلباتي</h1></div>
            <ul class="bread-crumb rtl">
                <li><a href="index.html">الرئيسية</a></li>
                <li>طلباتي</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- testimonial -->
<section class="testimonial tesimonial-page">
    <div class="container">
        <div class="testimonial-title centred">
            <div class="title-top">طلباتي</div>
            <div class="sec-title"><h2>تعرف على طلباتك و حالة كل طلب</h2></div>
        </div>
        <div class="row">

            <div class="col-md-6 col-sm-6 col-xs-12 testimonial-column">
                <div class="single-item rtl " >

                    <div class="catagories catagories2">
                        <div class="text2">  اسم المغسلة : </div>
                        <div class="list catagories2">
                            <p href="#"> مغسلة الأمانة </p>

                        </div>
                    </div>


                    <div class="catagories catagories2">
                        <div class="text2">  رقم الطلب : </div>
                        <div class="list catagories2">
                            <p href="#"> 5454548484800 </p>

                        </div>
                    </div>
                    <div class="catagories catagories2">
                        <div class="text2">  حالة الطلب : </div>
                        <div class="list catagories2">
                            <p href="#"> قيد التوصيل </p>

                        </div>
                    </div>
                    <div class="catagories catagories2">
                        <div class="text2">  تكلفة الطلب : </div>
                        <div class="list catagories2">
                            <p href="#"> 30 ريال سعودي </p>

                        </div>
                    </div>
                    <div class="catagories catagories2">
                        <div class="text2">  تاريخ الطلب : </div>


                        <div class="list catagories2">
                            <p href="#">24/11/1993 </p>

                        </div>

                    </div>

                    <a href="order-details.html" class="btn-one center">


                        تفاصيل الطلب</a>

                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 testimonial-column">
                <div class="single-item rtl " >

                    <div class="catagories catagories2">
                        <div class="text2">  اسم المغسلة : </div>
                        <div class="list catagories2">
                            <p href="#"> مغسلة الأمانة </p>

                        </div>
                    </div>


                    <div class="catagories catagories2">
                        <div class="text2">  رقم الطلب : </div>
                        <div class="list catagories2">
                            <p href="#"> 5454548484800 </p>

                        </div>
                    </div>
                    <div class="catagories catagories2">
                        <div class="text2">  حالة الطلب : </div>
                        <div class="list catagories2">
                            <p href="#"> فيد التنفيذ </p>

                        </div>
                    </div>
                    <div class="catagories catagories2">
                        <div class="text2">  تكلفة الطلب : </div>
                        <div class="list catagories2">
                            <p href="#"> 30 ريال سعودي </p>

                        </div>
                    </div>
                    <div class="catagories catagories2">
                        <div class="text2">  تاريخ الطلب : </div>


                        <div class="list catagories2">
                            <p href="#">24/11/1993 </p>

                        </div>

                    </div>

                    <a href="order-details.html" class="btn-one center">


                        تفاصيل الطلب</a>

                </div>
            </div>
        </div>

    </div>
</section>
<!-- testimonial end -->
@endsection
