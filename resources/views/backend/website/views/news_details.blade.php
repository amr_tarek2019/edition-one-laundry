@extends('backend.website.layouts.app')
@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('assets/website/assets/images/about/page-title.png')}});">
    <div class="container">
        <div class="content-box">
            <div class="title"><h1>الأخبار</h1></div>
            <ul class="bread-crumb rtl">
                <li><a href="index.html">الرئيسية</a></li>
                <li>الأخبار</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- service details -->
<section class="service-details">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 service-column">
                <div class="service-details-content">
                    <div class="row">

                        <div class="col-md- col-sm- col-xs-12">
                            <div class="img-box"><figure><img src="{{asset('assets/website/assets/images/service/d33.jpg')}}" alt=""></figure></div>
                        </div>
                    </div>
                    <div class="content-style-one rtl">
                        <div class="title">قريبا سيتم افتاح فرع جديد بالدوحة</div>
                        <div class="text">
                            <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال "lorem ipsum" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها
                                م استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال "lorem ipsum" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها
                                م استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال "lorem ipsum" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها
                                م استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال "lorem ipsum" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها

                                .</p>
                        </div>
                    </div>
                    <!--
                                            <div class="content-style-two rtl">
                                                <h3> Tincidunt Ante Enim</h3>
                                                <p>Excepteur sint occaecat cupidatat non proident sunt culpa qui officia deserunt mollit anim est labor. Sed perspiciatis unde omnis iste natus error.sit voluptatem accusantium doloremque laudantium totam rem aperiam eaque ipsa quae ab illo inventore verit atis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                                <div class="button"><a href="#" class="btn-one">Calculate Service</a></div>
                                            </div>
                    -->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
