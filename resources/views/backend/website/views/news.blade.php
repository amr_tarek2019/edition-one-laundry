@extends('backend.website.layouts.app')
@section('content')
<!-- Main slider -->
<section class="page-title centred" style="background-image: url({{asset('assets/website/assets/images/about/page-title.png')}});">
    <div class="container">
        <div class="content-box">
            <div class="title"><h1>الأخبار</h1></div>
            <ul class="bread-crumb rtl">
                <li><a href="index.html">الرئيسية</a></li>
                <li>الأخبار</li>
            </ul>
        </div>
    </div>
</section>
<!-- main-slider end -->





<!-- news section -->
<section class="news-section">
    <div class="container">
        <div class="news-title centred">
            <div class="title-top">أخبار المغسلة</div>
            <div class="sec-title"><h2>أخر الأخبار عن لاندرى ستيشن</h2></div>
        </div>
        <div class="row news-section2" >
            <div class="col-md-6 col-sm-6 col-xs-12 news-column">
                <div class="single-item text-right">
                    <div class="img-box"><a href="news-details.html"><figure><img src="{{asset('assets/website/assets/images/news/1.jpg') }}" alt=""></figure></a></div>
                    <div class="lower-content">
                        <h3><a href="news-details.html">قريبا اضافة خدمات التنظيف المنزلى</a></h3>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 news-column">
                <div class="single-item text-right">
                    <div class="img-box"><a href="news-details.html"><figure><img src="{{asset('assets/website/assets/images/news/2.jpg') }}" alt=""></figure></a></div>
                    <div class="lower-content">
                        <h3><a href="news-details.html">قريبا اضافة خدمات التنظيف المنزلى</a></h3>

                    </div>
                </div>
            </div>
        </div>
        <div class="row news-section2" >
            <div class="col-md-6 col-sm-6 col-xs-12 news-column">
                <div class="single-item text-right">
                    <div class="img-box"><a href="news-details.html"><figure><img src="{{asset('assets/website/assets/images/news/5.jpg') }}" alt=""></figure></a></div>
                    <div class="lower-content">
                        <h3><a href="news-details.html">قريبا اضافة خدمات التنظيف المنزلى</a></h3>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 news-column">
                <div class="single-item text-right">
                    <div class="img-box"><a href="news-details.html"><figure><img src="{{asset('assets/website/assets/images/news/6.jpg') }}" alt=""></figure></a></div>
                    <div class="lower-content">
                        <h3><a href="news-details.html">قريبا اضافة خدمات التنظيف المنزلى</a></h3>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- news section end -->
@endsection
