<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>laundry station</title>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-6jHF7Z3XI3fF4XZixAuSu0gGKrXwoX/w3uFPxC56OtjChio7wtTGJWRW53Nhx6Ev" crossorigin="anonymous">

    <link href="{{asset('assets/website/assets/css/all.css') }}" rel="stylesheet">
    <!--<link href="{{asset('assets/website/assets/css/style.css') }}" rel="stylesheet">-->
    <link href="{{asset('assets/website/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{asset('assets/website/assets/css/responsive.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" id="jssDefault" href="{{asset('assets/website/assets/css/custom/theme-2.css') }}"/>
    <link rel="icon" href="{{asset('assets/website/assets/images/favicon.ico') }}" type="image/x-icon">


</head>

<!-- page wrapper -->
<body class="boxed_wrapper">


<!-- .preloader -->
<div class="preloader"></div>
<!-- /.preloader -->







<!-- main header area -->
@include('backend.website.layouts.navbar')
<!-- end main header area -->


<!-- Main slider -->
@yield('content')


<!-- main footer area -->
@include('backend.website.layouts.footer')
<!-- main footer area -->



<!--End bodywrapper-->


<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-angle-up"></span></div>




<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title rtl" id="exampleModalLongTitle"> مغسلة الإخلاص و الأمل</h5>

            </div>
            <div class="modal-body">
                <div   id="oneordermap"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">إغلاق</button>

            </div>
        </div>
    </div>
</div>





<!--jquery js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBevTAR-V2fDy9gQsQn1xNHBPH2D36kck0"></script>

<script src="{{asset('assets/website/assets/js/jquery-2.1.4.js') }}"></script>
<script src="{{asset('assets/website/assets/js/bootstrap.min.js') }}"></script>
<script src="{{asset('assets/website/assets/js/owl.js') }}"></script>
<script src="{{asset('assets/website/assets/js/wow.js') }}"></script>
<script src="{{asset('assets/website/assets/js/validation.js') }}"></script>
<script src="{{asset('assets/website/assets/js/jquery-ui.js') }}"></script>
<script src="{{asset('assets/website/assets/js/jquery.fancybox.pack.js') }}"></script>
<script src="{{asset('assets/website/assets/js/SmoothScroll.js') }}"></script>
<script src="{{asset('assets/website/assets/js/jQuery.style.switcher.min.js') }}"></script>
<script src="{{asset('assets/website/assets/js/html5lightbox/html5lightbox.js') }}"></script>
<script src="{{asset('assets/website/assets/js/script.js') }}"></script>


<script src="{{asset('assets/website/assets/js/isotope.js') }}"></script>

<script src="{{asset('assets/website/assets/js/gmaps.js') }}"></script>
<script src="{{asset('assets/website/assets/js/map-helper.js') }}"></script>

<!-- End of .page_wrapper -->


<script>
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })

</script>

<script>
    var marker = null;
    function initMap() {
        var map = new google.maps.Map(document.getElementById('oneordermap'), {
            zoom: 7,
            center: {lat: 30.1187371, lng: 31.349201 }
        });
        var MaekerPos = new google.maps.LatLng(30.1187371 , 31.349201);
        marker = new google.maps.Marker({
            position: MaekerPos,
            map: map
        });
    }
</script>
<script async  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
</script>
<script>
    $(document).ready(function(){
        $('.count').prop('disabled', true);
        $(document).on('click','.plus',function(){
            $('.count').val(parseInt($('.count').val()) + 1 );
        });
        $(document).on('click','.minus',function(){
            $('.count').val(parseInt($('.count').val()) - 1 );
            if ($('.count').val() == 0) {
                $('.count').val(1);
            }
        });
    });
</script>
</body>


</html>
