<footer class="footer-area" style="background-image: url({{asset('assets/website/assets/images/home/footer.png') }});">
    <div class="subscribe-section">
        <div class="container">
            <div class="subscribe-content">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12 footer-column">
                        <div class="top-title"><h3>تابع الجديد من اخبارنا</h3></div>
                    </div>
                    <div class="col-md-9 col-sm-8 col-xs-12 footer-column">
                        <div class="footr-form rtl">
                            <form action="#" method="Post">
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="الاسم" required="">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" placeholder="البريد الالكتروني" required="">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn-one">تابعنا</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="main-footer">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12 footer-column">
                    <div class="logo-wideget footer-wideget">
                        <div class="footer-logo">
                            <a href="index.html"></a>
                        </div>
                        <div class="text">
                            <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ</p>
                        </div>
                        <ul class="footer-social text-right">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12 footer-column">
                    <div class="service-wideget footer-wideget">
                        <div class="footer-title"><h3>خدماتنا</h3></div>
                        <ul class="list">
                            <li><a href="dry-clean.html">غسيل</a></li>
                            <li><a href="dry-clean.html">كي</a></li>
                            <li><a href="dry-clean.html">غسيل و كي </a></li>
                            <li><a href="dry-clean.html">التوصيل للمنازل</a></li>
                            <li><a href="dry-clean.html">خدمات أخرى</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 footer-column">
                    <div class="link-wideget footer-wideget">
                        <div class="footer-title"><h3>روابط سريعة</h3></div>
                        <ul class="list">
                            <li><a href="About-us.html">عن لاندرى</a></li>
                            <li><a href="contact.html">اتصل بنا</a></li>
                            <li><a href="dry-clean.html">مغاسل بالقرب منك</a></li>
                            <li><a href="terms-condition.html">الشروط و الأحكام </a></li>
                            <li><a href="faq.html">الأسئلة الشائعة</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 footer-column">
                    <div class="contact-wideget footer-wideget">
                        <div class="footer-title"><h3>اتصل بنا</h3></div>
                        <div class="widget-content">
                            <div class="text">
                                <p>خدمة عملائنا 24 ساعة على مدار الأسبوع</p>
                            </div>
                            <ul class="contact-info">
                                <li><i class="icon fa fa-map-marker"></i>184 Main Rd E, St Albans VIC <br />3021, Australia</li>
                                <li><i class="icon fa fa-phone"></i><a href="tel:+22 657 325 413">+22 657 325 413</a></li>
                                <li><i class="icon fa fa-envelope"></i><a href="mailto:info@sharpes.com">info@sharpes.com</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="footer-bottom centred">
            <div class="copyright"> جميع الحقوق محفوظة لشركة © <a href="http://2grand.net/" class="grand" target="_blank">جراند رواد برمجة تطبيقات الجوال</a> </div>
        </div>
    </div>
</footer>
