<header class="main-header">

    <!-- header lower/fixed-header -->
    <div class="theme_menu stricky header-lower">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="logo-box">
                        <a href="index.html"></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="menu-bar">
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li class="current"><a href="index.html">الرئيسية</a></li>
                                    <li ><a href="About-us.html">عن لاندرى </a>

                                    </li>
                                    <li class="dropdown"><a href="#">الخدمات</a>
                                        <ul class="submenu">
                                            <li><a href="dry-clean.html">كي</a></li>
                                            <li><a href="dry-clean.html">غسيل</a></li>
                                            <li><a href="dry-clean.html">غسيل و كي بالبخار</a></li>
                                            <li><a href="dry-clean.html">توصيل الطلبات للمنازل</a></li>
                                        </ul>
                                    </li>

                                    <li ><a href="news.html">الأخبار</a>

                                    </li>
                                    <li><a href="contact.html">تواصل معنا</a></li>
                                    <li><a href="request.html">طلب إنضمام</a></li>
                                </ul>

                                <!-- mobile menu -->
                                <ul class="mobile-menu clearfix">

                                    <li ><a href="index.html">الرئيسية</a></li>
                                    <li ><a href="About-us.html">عن لاندرى </a>

                                    </li>
                                    <li class="dropdown"><a href="#">الخدمات</a>
                                        <ul class="submenu">
                                            <li><a href="dry-clean.html">كي</a></li>
                                            <li><a href="dry-clean.html">غسيل</a></li>
                                            <li><a href="dry-clean.html">غسيل و كي بالبخار</a></li>
                                            <li><a href="dry-clean.html">توصيل الطلبات للمنازل</a></li>
                                        </ul>
                                    </li>

                                    <li ><a href="news.html">الأخبار</a>

                                    </li>
                                    <li><a href="contact.html">تواصل معنا</a></li>
                                    <li><a href="request.html">طلب إنضمام</a></li>

                                </ul>
                            </div>

                        </nav>
                        <div class="cart-box ">
                            <a href="my-orders.html"><i class="fas fa-dumpster" aria-hidden="true">


                                </i>طلباتك</a>
                        </div>
                        <div class="cart-box cart-box2">
                            <a href="login.html"><i class="fas fa-user-tie" aria-hidden="true">


                                </i>حسابي</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end header lower/fixed-header -->

</header>
