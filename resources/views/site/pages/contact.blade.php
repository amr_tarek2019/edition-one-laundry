@extends('site.layouts.master')

@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('site/images/about/page-title.png')}});">
    <div class="container">
        <div class="content-box rtl">
            <div class="title">
                <h1>اتصل بنا</h1>
            </div>
            <ul class="bread-crumb">
                <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                <li>إتصل بنا</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- contact section -->
<section class="contact-section">
    <div class="contact-form-area">
        <div class="container">
            <div class="title-top centred">إرسل رسالتك</div>
            <div class="sec-title centred">
                <h2>كن على تواصل معنا دائما</h2>
            </div>
            <form id="contact-form" name="contact_form" class="default-form"
                action="{{ route('site.home.contact.post') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="text" name="title" required value="" placeholder='عنوان الرسالة' required="">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <textarea placeholder="الرسالة" name="details" required=""></textarea>
                    </div>
                </div>
                <div class="contact-btn centred"><button type="submit" class="btn-one"
                        data-loading-text="Please wait...">إرسال </button></div>
            </form>
        </div>
    </div>
</section>
<!-- contact section end -->
@endsection