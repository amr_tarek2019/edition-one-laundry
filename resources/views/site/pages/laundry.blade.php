@extends('site.layouts.master')
@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('site/images/about/page-title.png')}});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>جميع مغاسلنا</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                <li>المغاسل</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- service section -->
<section class="service-section sec-pad">
    <div class="container">
        <div class="service-title centred">
            <div class="title-top">جميع المغاسل</div>
            <div class="sec-title">
                <h2>دائما نكون فى خدمتك</h2>
            </div>
        </div>
        <div class="row">
            @foreach ($laundries as $item)
              <div class="col-md-4 col-sm-6 col-xs-12 service-column">
                <div class="single-item">
                    <div class="single-item-overlay">
                        <div class="img-box">
                            <figure><img src="{{ $item->img }}" alt=""></figure>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="content">
                                        <li><a href="{{ route('site.laundries.details', $item->id) }}"><i class="fa fa-link"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lower-content rtl">
                        <h3><a href="{{ route('site.laundries.details', $item->id) }}">{{ $item->name }}</a></h3>
                        <div class="text">{{str_limit($item->description,105)}}</div>
            
                    </div>
            
                </div>
            </div>  
            @endforeach
           
        </div>
    </div>
</section>
<!-- service section end -->
@endsection