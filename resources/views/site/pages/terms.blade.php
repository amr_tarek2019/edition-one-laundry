@extends('site.layouts.master')

@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{ asset('site/images/about/page-title.png') }});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>الشروط و الأحكام</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                <li>الشروط و الأحكام</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- service details -->
<section class="service-details">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 service-column">
                <div class="service-details-content">
                    <div class="row">

                        <div class="col-md- col-sm- col-xs-12">
                            <div class="img-box">
                                <figure><img src="{{ asset('site/images/service/d22.jpg') }}" alt=""></figure>
                            </div>
                        </div>
                    </div>
                    <div class="content-style-one rtl">
                        <div class="title">الشروط و الأحكام</div>
                        <div class="text">
                            <p>{{ $terms->terms }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- service details end -->

@stop