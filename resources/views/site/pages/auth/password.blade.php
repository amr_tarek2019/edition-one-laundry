@extends('site.layouts.master')

@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('site/images/about/page-title2.png')}});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>تغيير كلمة المرور</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.index') }}">الرئيسية</a></li>
                <li>تغيير كلمة المرور</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- contact section -->
<section class="contact-section">

    <div class="contact-form-area">
        <div class="container">
            <div class="title-top centred">إستعادة حسابك </div>
            <div class="sec-title centred">
                <h2>من فضلك ادخل كلمة مرور جديدة </h2>
            </div>
            <form id="contact-form" name="contact_form" class="default-form" action="{{ route('site.auth.password.post', ['phone'=>$phone]) }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="password" required name="password" value="" placeholder="كلمة المرور">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="password" required name="password_confirmation" value="" placeholder="تأكيد كلمة المرور">
                    </div>
                </div>
                <div class="contact-btn centred"><button type="submit" class="btn-one"
                        data-loading-text="Please wait..."> إستعادة حسابي</button></div>

            </form>
        </div>
    </div>
</section>
<!-- contact section end -->
@endsection