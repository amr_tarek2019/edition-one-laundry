@extends('site.layouts.master')

@section('content')
<!-- page title -->
<section class="page-title centred" style="background-image: url({{asset('site/images/about/page-title2.png')}});">
    <div class="container">
        <div class="content-box">
            <div class="title">
                <h1>إنشاء حساب جديد</h1>
            </div>
            <ul class="bread-crumb rtl">
                <li><a href="{{ route('site.home.about') }}">الرئيسية</a></li>
                <li>إنشاء حساب</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->


<!-- contact section -->
<section class="contact-section">

    <div class="contact-form-area">
        <div class="container">
            <div class="title-top centred">ليس لديك حساب</div>
            <div class="sec-title centred">
                <h2>من فضلك قم بإنشاء حساب جديد </h2>
            </div>
            <form id="contact-form" name="contact_form" class="default-form" action="{{ route('site.auth.register.post') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="text" name="name" required value="" placeholder=" الأسم">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="email" required name="email" value="" placeholder="البريد الإلكتروني">
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="password" required name="password" value="" placeholder="كلمة المرور">
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-centered">
                        <input type="phone" required name="phone" value="" placeholder="الهاتف ">
                    </div>

                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 col-centered" style="direction: rtl">
                    <div class="row">
                        <div class="form-group">
                            <label for="address_address">العنوان</label>
                            <input required type="text" id="address-input" name="address" class="form-control map-input">
                            <input type="hidden" name="lat" id="address-latitude" value="0" />
                            <input type="hidden" name="lng" id="address-longitude" value="0" />
                        </div>
                        <div id="address-map-container" style="width:100%;height:400px; ">
                            <div style="width: 100%; height: 100%" id="address-map"></div>
                        </div>
                    </div>
                </div>
                <div class="contact-btn centred"><button type="submit" class="btn-one"
                        data-loading-text="Please wait...">إنشاء حساب</button></div>



                <div class="option-block col-centered">
                    <div class="radio-block">
                        <div class="checkbox">
                            <label>

                                <a href="login.html"> تسجيل الدخول ؟ </a>
                            </label>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- contact section end -->
@endsection

@section('scripts')
@parent
<script
    src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize"
    async defer></script>
<script src="{{asset('js/map.js')}}"></script>
@stop