@extends('laundry.layouts.master')

@section('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('messenger/css/messenger.css')}}" media="all" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            {{-- <div class="col-md-3 threads">
                 @include('messenger::partials.threads')
             </div>--}}

            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><h4>{{$withUser->name}}</h4></div>
                    <div class="panel-body">
                        <div class="messenger">
                            @if( is_array($messages) )
                                @if (count($messages) === 20)
                                    <div id="messages-preloader"></div>
                                @endif

                                <div id="messages-preloader"></div>
                            @else
                                <p class="start-conv">Conversation started</p>
                            @endif
                            <div class="messenger-body">
                                @include('laundry.messenger.partials.messages')
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <input type="hidden" name="receiverId" value="{{$withUser->id}}">
                        <textarea id="message-body" name="message" rows="2"
                                  placeholder="Type your message..."></textarea>
                        <button type="submit" id="send-btn" class="btn btn-primary">SEND</button>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><h4>المشرفين</h4></div>

                    <div class="panel-body" style="height: 500px;overflow-y: scroll;">
                        @foreach($admins as $admin)
                            <ul>
                                <li>
                                    <a href="{{route('laundry.messenger',$admin->id)}}">{{$admin->name}} </a>
                                </li>
                            </ul>
                        @endforeach
                        {{--  <p>
                              <span>Name</span> {{$withUser->name}}
                          </p>
                          <p>
                              <span>Email</span> {{$withUser->email}}
                          </p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{--    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>--}}
    <script type="text/javascript">
        var withId = {{$withUser->id}},
            authId = {{auth('laundry')->id()}},
            withType = "Admin",
            messagesCount = {{is_array($messages) ? count($messages) : '0'}};
        pusher = new Pusher('{{config('messenger.pusher.app_key')}}', {
            cluster: '{{config('messenger.pusher.options.cluster')}}'
        });
    </script>
    <script type="text/javascript" src="{{asset('messenger/js/messenger-chat.js')}}"></script>
@endsection
