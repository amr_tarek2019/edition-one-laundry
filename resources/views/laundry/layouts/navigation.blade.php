<div class="br-logo">
    <a target="_blank" href="http://2grand.net/">
        <span>
        <img style="max-width: 90% !important;" src="{{asset('/android.png')}}" class="grand">
        </span>
    </a>
</div>
<div class="br-sideleft overflow-y-auto">
    <label class="sidebar-label pd-x-15 mg-t-20" style="direction: rtl">القائمه الرئيسيه</label>
    <div class="br-sideleft-menu">
        <a href="{{route('laundry.dashboard.index')}}"
           class="br-menu-link {{sidebarActive(['laundry.dashboard.index'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">الرئيسيه</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->

        <a href="{{route('laundry.laundries.user.details' , auth('laundry')->id())}}"
           class="br-menu-link {{sidebarActive(['laundry.laundries.user.details'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-person-outline tx-24"></i>
                <span class="menu-item-label"> المغسله</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->


        <a href="{{route('laundry.messenger',1)}}" class="br-menu-link {{sidebarActive(['laundry.messenger'])}}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-chatbox tx-24"></i>
                <span class="menu-item-label"> المحادثات </span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->

    </div><!-- br-sideleft-menu -->

</div><!-- br-sideleft -->