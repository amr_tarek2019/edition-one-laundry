@extends('admin.layouts.master')

@section('title')

    الاخبار

@stop

@section('content')

    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.dashboard.index')}}">الرئيسيه</a>
            {{--                <a class="breadcrumb-item" href="{{route('row.showindex')}}">الفئات</a>--}}
            <span class="breadcrumb-item active">الاخبار</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">جدول الاخبار </h4>
        <p class="mg-b-0">عرض كل الاخبار </p>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            @can('add_news')
                <div class="col-md-4">
                    <div class="col-md-5 mg-b-15">
                        <a href="" class="btn btn-success btn-with-icon btn-block" data-toggle="modal"
                           data-target="#modaldemo">
                            <div class="ht-40 justify-content-between">
                            <span class="icon wd-40">
                                <i class="fa fa-plus"></i></span>
                                <span class="pd-x-15">اضافه </span>
                            </div>
                        </a>
                    </div>
                </div>
            @endcan
            @include('errors.custom-messages')
            @include('errors.validation-errors')
            <div class="table-wrapper">
                <table id="datatable1" class=" display  nowrap" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="wd-15p all">العنوان</th>
                        <th class="wd-15p all">الصوره</th>
                        <th class="wd-15p all">التفاصيل</th>
                        @canany(['edit_news','delete_news'])
                            <th class="wd-20p all">المزيد</th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rows as  $row)
                        <tr id="{{$row->id}}">
                            <td>{{$row->title}}</td>
                            <td>

                            <img src="{{$row->img}}" width="100px" alt="" srcset="">
                            </td>
                            <td><a href="" class="btn btn-info tx-11 tx-uppercase pd-y-11 pd-x-15 tx-mont tx-medium" data-toggle="modal"
                                data-target="#modaldemozz{{$row->id}}">تفاصيل
                                </a></td>
                            @canany(['edit_news','delete_news'])
                                <td>
                                    @can('edit_news')
                                        <a href="" data-toggle="modal"
                                           data-target="#modaldemoo{{$row->id}}"
                                           class="btn btn-info btn-icon mg-r-5 mg-b-10">
                                            <div><i class="fa fa-edit"></i></div>
                                        </a>
                                    @endcan
                                    @can('delete_news')
                                        <a href="#"
                                           onclick="showDeletConfirmationModal('{{route('admin.news.news.delete', $row->id)}}'  , {{$row->id}})"
                                           class="btn btn-danger btn-icon mg-r-5 mg-b-10">
                                            <div><i class="fa fa-trash-o"></i></div>
                                        </a>
                                    @endcan
                                </td>
                            @endcan
                        </tr>
                        <div id="modaldemozz{{$row->id}}" class="modal fade">
                                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl ; width: 600px">
                                            <div class="modal-content tx-size-sm">
                                                <div class="modal-header pd-x-20">
                                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                                        التفاصيل</h6>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body pd-20">
                                              <textarea  disabled class="form-control" cols="30" rows="10">{{$row->description}}</textarea>
                                                </div><!-- modal-body -->
                                                <div class="modal-footer" style="direction: rtl">
                                                    <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">غلق
                                                    </button>
                                                </div>
                                            </div>
                                        </div><!-- modal-dialog -->
                                    </div><!-- modal -->
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->


        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    @can('add_news')
        <div id="modaldemo" class="modal fade">
            <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">اضافه </h6>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="adddelegate" action="{{route('admin.news.add.news')}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body pd-20">
                            <div class="row mg-b-30">
                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <input class="form-control" required placeholder="العنوان" type="text" name="title"
                                           data-parsley-required-message="برجاء إدخال العنوان">
                                    @include('errors.validation-messages', ['field' => 'title'])
                                </div><!-- col-4 -->
                                <div class="col-lg-6 mg-b-30 " style="text-align: center">
                                    <label class="custom-file">
                                        <input type="file" id="file" name="img" class="custom-file-input"
                                               required
                                               data-parsley-required-message="برجاء إختيار الصوره">
                                        <span class="custom-file-control">الصوره</span>
                                    </label>
                                    @include('errors.validation-messages', ['field' => 'img'])
                                </div><!-- col -->
                                <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                    <textarea name="description" requiredclass="form-control" id="" cols="100" rows="10" data-parsley-required-message="برجاء إدخال التفاصيل" placeholder="التفاصيل"></textarea>
                                    @include('errors.validation-messages', ['field' => 'description'])
                                </div><!-- col-4 -->
                            </div>
                        </div><!-- modal-body -->
                        <div class="modal-footer" style="direction: rtl">
                            <button type="submit" class="btn btn-primary tx-size-xs"
                            >حفظ
                            </button>
                            <button type="button" class="btn btn-secondary tx-size-xs"
                                    data-dismiss="modal">غلق
                            </button>
                        </div>
                    </form>
                </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->
    @endcan
    @can('edit_news')
        @foreach($rows as $row)
            <div id="modaldemoo{{$row->id}}" class="modal fade">
                <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                    <div class="modal-content tx-size-sm">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">تعديل </h6>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="editdelegate" action="{{route('admin.news.edit.news',['id'=>$row->id])}}"
                              method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body pd-20">
                                <div class="row mg-b-30">
                                    <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                        <input class="form-control" required placeholder="العنوان" type="text" name="title"
                                               value="{{$row->title}}"
                                               data-parsley-required-message="برجاء إدخال العنوان">
                                        @include('errors.validation-messages', ['field' => 'title'])
                                    </div><!-- col-4 -->
                            <div class="col-lg-6 mg-b-30 " style="text-align: center">
                                <label class="custom-file">
                                    <input type="file" id="file" name="img" class="custom-file-input" 
                                        data-parsley-required-message="برجاء إختيار الصوره">
                                    <span class="custom-file-control">الصوره</span>
                                </label>
                                @include('errors.validation-messages', ['field' => 'img'])
                            </div><!-- col -->
                            <div class="col-lg-6 mg-t-20 mg-lg-t-0 mg-b-30">
                                <textarea name="description" requiredclass="form-control" id="" cols="100" rows="10"
                                    data-parsley-required-message="برجاء إدخال التفاصيل" placeholder="التفاصيل">{{$row->description}}</textarea>
                                @include('errors.validation-messages', ['field' => 'description'])
                            </div><!-- col-4 -->
                                </div>
                            </div><!-- modal-body -->
                            <div class="modal-footer" style="direction: rtl">
                                <button type="submit" class="btn btn-primary tx-size-xs"
                                >حفظ
                                </button>
                                <button type="button" class="btn btn-secondary tx-size-xs"
                                        data-dismiss="modal">غلق
                                </button>
                            </div>
                        </form>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->
        @endforeach
    @endcan
@stop

@section('scripts')
    <script>
        $('#adddelegate').parsley();
        $('#editdelegate').parsley();
    </script>
@stop