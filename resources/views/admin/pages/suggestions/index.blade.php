@extends('admin.layouts.master')

@section('title')

    المقترحات

@stop

@section('content')

    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.dashboard.index')}}">الرئيسيه</a>
            <span class="breadcrumb-item active"> الاقتراحات  </span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">جدول الاقتراجات</h4>
        <p class="mg-b-0">عرض كل الاقتراحات </p>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                <table id="datatable1" class=" display  nowrap" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="wd-15p all">المستخدم</th>
                        <th class="wd-15p all">العنوان</th>
                        <th class="wd-15p all">التفاصيل</th>
                        @can('delete_suggestions')
                            <th class="wd-15p all">الإجراء</th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($suggestions as  $suggestion)
                            <tr id="{{$suggestion->id}}">
                                <td><a href="{{route('admin.users.user.details',$suggestion->user->id)}}">{{$suggestion->user->phone}} </a> </td>
                                <td>{{$suggestion->title}}</td>
                                <td><a href=""
                                       class="btn btn-info tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"
                                       data-toggle="modal" data-target="#modaldemooozz{{$suggestion->id}}">عرض
                                        </a></td>
                                @can('delete_suggestions')
                                    <td>
                                        <a href="#"
                                           onclick="showDeletConfirmationModal('{{route('admin.suggestions.suggestion.delete', $suggestion->id)}}'  , {{$suggestion->id}})"
                                           class="btn btn-danger btn-icon mg-r-5 mg-b-10">
                                            <div><i class="fa fa-trash-o"></i></div>
                                        </a>
                                    </td>
                                @endcan
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
            @foreach($suggestions as  $suggestion)
                <!-- LARGE MODAL -->
                    <div id="modaldemooozz{{$suggestion->id}}" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">عرض التفاصيل</h6>
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pd-20">
                                    <input class="form-control" placeholder="{{$suggestion->details}}"
                                           type="text"
                                           readonly>
                                </div><!-- modal-body -->
                                <div class="modal-footer" style="direction: rtl">
                                    <button type="button" class="btn btn-secondary tx-size-xs"
                                            data-dismiss="modal">غلق
                                    </button>
                                </div>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                @endforeach
        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
@stop