@extends('admin.layouts.master')

@section('title')

    المدن

@stop

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.dashboard.index')}}">الرئيسيه</a>
            <span class="breadcrumb-item active">المدن</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">جدول المدن</h4>
        <p class="mg-b-0">عرض المدن </p>
    </div>


    <div class="br-pagebody">
        <div class="br-section-wrapper">
            @can('add_cities')
                <div class="col-md-6">
                    <div class="col-md-5 mg-b-15">
                        <a href="" class="btn btn-success btn-with-icon btn-block" data-toggle="modal"
                           data-target="#modaldemo">
                            <div class="ht-40 justify-content-between">
                            <span class="icon wd-40">
                                <i class="fa fa-plus"></i></span>
                                <span class="pd-x-15">اضافه مدينه</span>
                            </div>
                        </a>
                    </div>
                </div>
                @include('errors.validation-errors')
                @include('errors.custom-messages')
            @endcan
            <div class="col-md-6"></div>
            <div class="table-wrapper">
                <table id="datatable1" class=" display  nowrap" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="wd-15p">المدينه</th>
                        <th class="wd-15p">city</th>
                        <th class="wd-15p">الشحن</th>
                        <th class="wd-20p all">المزيد</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cities as  $city)
                        <tr id="{{$city->id}}">
                            <td>{{$city->name}}</td>
                            <td>{{$city->name_en}}</td>
                            <td>{{$city->delivery}}</td>
                            <td>
                                <a href="" data-toggle="modal"
                                   data-target="#modaldemoo{{$city->id}}"
                                   class="btn btn-info btn-icon mg-r-5 mg-b-10">
                                    <div><i class="fa fa-edit"></i></div>
                                </a>
                                @can('delete_cities')
                                    <a href="#"
                                       onclick="showDeletConfirmationModal('{{route('admin.cities.delete', $city->id)}}'  , {{$city->id}})"
                                       class="btn btn-danger btn-icon mg-r-5 mg-b-10">
                                        <div><i class="fa fa-trash-o"></i></div>
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->

        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    @can('add_cities')
        <div id="modaldemo" class="modal fade">
            <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">اضافه مدينه</h6>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="sendNotification" action="{{route('admin.cities.add')}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body pd-20">
                            <div class="row mg-b-30">
                                <div class="col-lg-4 mg-b-30 ">
                                    <input class="form-control" required placeholder="الإسم" type="text" name="name"
                                           data-parsley-required-message="برجاء إدخال الإسم">
                                    @include('errors.validation-messages', ['field' => 'name'])

                                </div><!-- col --> 
                                <div class="col-lg-4 mg-b-30 ">
                                    <input class="form-control" required placeholder="english name" type="text" name="name_en"
                                           data-parsley-required-message="please enter english name">
                                    @include('errors.validation-messages', ['field' => 'name_en'])

                                </div><!-- col -->
                                <div class="col-lg-4 mg-b-30 ">
                                    <input class="form-control" required placeholder="الشحن" type="number"
                                           name="delivery"
                                           data-parsley-required-message="برجاء إدخال الشحن">
                                    @include('errors.validation-messages', ['field' => 'delivery'])

                                </div><!-- col -->

                            </div>
                        </div><!-- modal-body -->
                        <div class="modal-footer" style="direction: rtl">
                            <button type="submit" class="btn btn-primary tx-size-xs"
                            >إرسال
                            </button>
                            <button type="button" class="btn btn-secondary tx-size-xs"
                                    data-dismiss="modal">غلق
                            </button>
                        </div>
                    </form>
                </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->
    @endcan
    @can('edit_cities')
        @foreach($cities as $city)
            <div id="modaldemoo{{$city->id}}" class="modal fade edit_me">
                <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                    <div class="modal-content tx-size-sm">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">تعديل مدينه</h6>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="editdelegate" action="{{route('admin.cities.edit',['id'=>$city->id])}}"
                              method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body pd-20">
                                <div class="row mg-b-30">
                                    <div class="col-lg-4 mg-b-30 ">
                                        <input class="form-control" required placeholder="الإسم" type="text" name="name"
                                               data-parsley-required-message="برجاء إدخال الإسم"
                                               value="{{$city->name}}">
                                        @include('errors.validation-messages', ['field' => 'name'])

                                    </div><!-- col -->
                                    <div class="col-lg-4 mg-b-30 ">
                                        <input class="form-control" required placeholder="english name" type="text" name="name_en"
                                               data-parsley-required-message="please enter english name"
                                               value="{{$city->name_en}}">
                                        @include('errors.validation-messages', ['field' => 'name_en'])

                                    </div><!-- col -->
                                    <div class="col-lg-4 mg-b-30 ">
                                        <input class="form-control" required placeholder="الشحن" type="number"
                                               name="delivery"
                                               data-parsley-required-message="برجاء إدخال الشحن"
                                               value="{{$city->delivery}}">
                                        @include('errors.validation-messages', ['field' => 'delivery'])

                                    </div><!-- col -->
                                </div>
                            </div><!-- modal-body -->
                            <div class="modal-footer" style="direction: rtl">
                                <button type="submit" class="btn btn-primary tx-size-xs"
                                >حفظ
                                </button>
                                <button type="button" class="btn btn-secondary tx-size-xs"
                                        data-dismiss="modal">غلق
                                </button>
                            </div>
                        </form>
                    </div>
                </div><!-- modal-dialog -->
            </div><!-- modal -->
        @endforeach
    @endcan
@endsection
@section('scripts')
    <script>
        $('#sendNotification').parsley();
    </script>
    <script>
        $('.edit_me').parsley();
    </script>
@stop