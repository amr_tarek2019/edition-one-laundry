@extends('admin.layouts.master')

@section('title')

    الطلبات

@stop

@section('styles')
    <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" media="all" rel="stylesheet"
          type="text/css"/>
@stop
@section('content')

    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.dashboard.index')}}">الرئيسيه</a>
            {{--                <a class="breadcrumb-item" href="{{route('user.showindex')}}">الفئات</a>--}}
            <span class="breadcrumb-item active">الطلبات</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">جدول الطلبات </h4>
        <p class="mg-b-0">عرض كل الطلبات </p>
    </div>
@include('errors.custom-messages')
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            <div class="table-wrapper">
                <p id="date_filter" class="col-sm-12" style="display: flex ; justify-content: space-between">
                    <span id="date-label-from" class="date-label">من : <input name="min" id="min" type="text"> </span>
                    <span id="date-label-to" class="date-label">إلى :<input name="max" id="max" type="text"> </span>
                </p>
                <table id="orders_table" class=" display  nowrap" style="width: 100%">
                    <thead>
                    <tr>
                        <th  class="wd-15p all print">رقم الطلب</th>
                        <th class="wd-15p all print">تاريخ الطلب</th>
                        <th class="wd-15p all " style="text-align: center ;">التفاصيل</th>
                        <th class="wd-15p all">المستخدم</th>
                        <th class="wd-15p all print">المستخدم</th>
                        <th class="wd-15p all">المندوب</th>
                        <th class="wd-15p all print">المندوب</th>
                        <th class="wd-15p all ">حاله الطلب</th>
                        <th class="wd-15p all ">سبب الإلغاء</th>
                        <th class="wd-15p all print">المدينه</th>
                        <th class="wd-15p all print">الشحن</th>
                           <th class="wd-15p all print">وقت التسليم</th>
                        <th class="wd-15p all print">الإجمالى</th>
                        <th class="wd-15p all">تفاصيل الفاتوره</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($orders))
                        @foreach($orders as  $order)
                            <tr id="{{$order->id}}">
                                <td>
                                    
                                    @if(!empty($order->user->id))
                                    <a href="{{route('admin.users.user.details',$order->user->id)}}"> {{$order->id}} </a>
                                    @else
                                    <h4>لا توجد بيانات للمستخدم</h4>
                                    @endif
                                </td>
                                <td>{{$order->created_at}}</td>
                                <td>
                                    <a href=""
                                       class="btn btn-info tx-11 tx-uppercase pd-y-11 pd-x-15 tx-mont tx-medium"
                                       data-toggle="modal" data-target="#modaldemooo{{$order->id}}">تفاصيل
                                        الطلب</a>
                                </td>
                                <td>
                                        <a href=""
                                           class="btn btn-info tx-11 tx-uppercase pd-y-11 pd-x-15 tx-mont tx-medium"
                                           data-toggle="modal" data-target="#modaldemozz{{$order->id}}">تفاصيل
                                            المستخدم</a>
                                </td>
                          
                                <td>
                                   <b>الإسم : </b> {{!empty($order->user->name)?$order->user->name:'بيانات المستخدم غير موجودة'}} | <b> الهاتف :</b> {{!empty($order->user->phone) ? $order->user->phone : 'بيانات المستخدم غير موجودة '}}                               </td>
                                <td>
                                    @if ($order->delegate_id)
                                        <a href=""
                                           class="btn btn-info tx-11 tx-uppercase pd-y-11 pd-x-15 tx-mont tx-medium"
                                           data-toggle="modal" data-target="#modaldemo{{$order->id}}">تفاصيل
                                            المندوب</a>
                                    @else
                                        لا يوجد
                                    @endif

                                </td>

                                <td>
                                    <b>الإسم : </b> {{$order->delegate->name}} | <b>الهاتف : </b> {{$order->delegate->phone}}
                                </td>
                                <td>
                                    @if ($order->order_status == 0)
                                        طلب جديد
                                    @elseif ($order->order_status == 1)
                                        إلى الطريق للمغسله
                                    @elseif ($order->order_status == 2)
                                        تم التسليم للمغسله
                                    @elseif ($order->order_status == 3)
                                        إلى الطريق للعميل
                                    @elseif ($order->order_status == 4)
                                        مكتمل
                                    @elseif ($order->order_status == 5)
                                        ملغى
                                    @endif
                                </td>
                                <td>{{$order->order_status == 5 ? $order->reason : 'غير ملغى'}}</td>
                                <td>{{$order->city }}</td>
                                <td>{{$order->delivery}}</td>
                                 <td>{{$order->estimate_time}}</td>
                                <td>{{$order->totalPrice()}}</td>
                                <td>
                                    <form action="{{route('admin.orders.invoice.download' , $order->id)}}" method="post">
                                        @csrf
                                        <button type="submit" style="padding: 12px;"
                                                class="btn btn-warning btn-icon mg-r-5 mg-b-10">
                                            <i class="fa fa-cloud-download"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        @if (count($orders))
            @foreach($orders as  $order)
                <!-- LARGE MODAL -->
                    <div id="modaldemooo{{$order->id}}" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document"
                             style="direction: rtl ; width: 600px">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">الخدمات</h6>
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pd-20">
                                    <table style="width: 100%" class="table">
                                        <thead>
                                        <tr>
                                            <th>الإسم</th>
                                            <th>النوع</th>
                                            <th>الكميه</th>
                                            <th>التكلفه</th>
                                        </tr>
                                        </thead>
                                        @foreach($order->getOrderServices() as $service)
                                            <tbody>
                                            <tr>
                                                <td>{{$service['service_name']}}</td>
                                                <td>{{$service['service_type']}}</td>
                                                <td>{{$service['service_count']}}</td>
                                                <td>{{$service['service_price']}}</td>
                                            </tr>
                                            </tbody>
                                        @endforeach
                                    </table>
                                </div><!-- modal-body -->
                                <div class="modal-footer" style="direction: rtl">
                                    <button type="button" class="btn btn-secondary tx-size-xs"
                                            data-dismiss="modal">غلق
                                    </button>
                                </div>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                    <!-- LARGE MODAL -->
                    <div id="modaldemozz{{$order->id}}" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document"
                             style="direction: rtl ; width: 600px">
                            <div class="modal-content tx-size-sm">
                                <div class="modal-header pd-x-20">
                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                        المستخدم</h6>
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body pd-20">
                                    <table style="width: 100%" class="table">
                                        <thead>
                                        <tr>
                                            <th>الإسم</th>
                                            <th>البريد الإلكترونى</th>
                                            <th>الهاتف</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{!empty($order->user->name)?$order->user->name:'بيانات المستخدم غير موجودة'}}</td>
                                            <td>{{!empty($order->user->email)?$order->user->email:'بيانات المستخدم غير موجودة'}}</td>
                                            <td>{{!empty($order->user->phone)?$order->user->phone:'بيانات  المستخدم غير موجودة'}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!-- modal-body -->
                                <div class="modal-footer" style="direction: rtl">
                                    <button type="button" class="btn btn-secondary tx-size-xs"
                                            data-dismiss="modal">غلق
                                    </button>
                                </div>
                            </div>
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                @if ($order->delegate_id)
                    <!-- LARGE MODAL -->
                        <div id="modaldemo{{$order->id}}" class="modal fade">
                            <div class="modal-dialog modal-lg" role="document"
                                 style="direction: rtl ; width: 600px">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                            المندوب</h6>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <table style="width: 100%" class="table">
                                            <thead>
                                            <tr>
                                                <th>الإسم</th>
                                                <th>البريد الإلكترونى</th>
                                                <th>الهاتف</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>{{$order->delegate->name}}</td>
                                                <td>{{$order->delegate->email}}</td>
                                                <td>{{$order->delegate->phone}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div><!-- modal-body -->
                                    <div class="modal-footer" style="direction: rtl">
                                        <button type="button" class="btn btn-secondary tx-size-xs"
                                                data-dismiss="modal">غلق
                                        </button>
                                    </div>
                                </div>
                            </div><!-- modal-dialog -->
                        </div><!-- modal -->
                        <!-- LARGE MODAL -->
                    @endif
                @endforeach
            @endif
        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->

@stop

@section('scripts')

    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="{{asset('cpanel/js/orders.js')}}"></script>

@stop