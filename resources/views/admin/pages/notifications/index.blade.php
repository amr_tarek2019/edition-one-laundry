@extends('admin.layouts.master')

@section('title')

    الإشعارات

@stop

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.dashboard.index')}}">الرئيسيه</a>
            <span class="breadcrumb-item active">الخدمات</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">جدول الإشعارات</h4>
        <p class="mg-b-0">عرض كل الإشعارات </p>
    </div>
    <div class="br-pagebody">
        <div class="br-section-wrapper">
            @can('add_notification')
                <div class="col-md-6">
                    <div class="col-md-5 mg-b-15">
                        <a href="" class="btn btn-success btn-with-icon btn-block" data-toggle="modal"
                           data-target="#modaldemo">
                            <div class="ht-40 justify-content-between">
                            <span class="icon wd-40">
                                <i class="fa fa-plus"></i></span>
                                <span class="pd-x-15">اضافه إشعار</span>
                            </div>
                        </a>
                    </div>
                </div>
            @endcan
            @include('errors.custom-messages')
            <div class="col-md-6"></div>
            <div class="table-wrapper">
                <table id="datatable1" class=" display  nowrap" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="wd-15p">العنوان</th>
                        <th class="wd-15p">التفاصيل</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($notifications as  $notification)
                        <tr id="{{$notification->id}}">
                            <td>{{$notification->title}}</td>
                            <td>{{$notification->body}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->

        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->
    @can('add_notification')
        <div id="modaldemo" class="modal fade">
            <div class="modal-dialog modal-lg" role="document" style="direction: rtl">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">اضافه إشعار</h6>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="sendNotification" action="{{route('admin.notifications.send')}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body pd-20">
                            <div class="row mg-b-30">
                                <div class="col-lg-12 mg-b-30 ">
                                    <label for="receiver">المستقبل</label>
                                    <select class="form-control" name="type" id="receiver" required
                                            data-parsley-required-message="برجاء إختيار المستقبل">
                                        <option value="all">all</option>
                                        <option value="users">users</option>
                                        <option value="delegates">delegates</option>
                                    </select>

                                    @include('errors.validation-messages', ['field' => 'type'])

                                </div><!-- col -->
                                <div class="col-lg-12 mg-b-30 ">
                                    <input class="form-control" required placeholder="العنوان" type="text" name="title"
                                           data-parsley-required-message="برجاء إدخال العنوان">
                                    @include('errors.validation-messages', ['field' => 'title'])

                                </div><!-- col -->
                                <div class="col-lg-12 ">
                                <textarea class="form-control" name="body" placeholder="التفاصيل" cols="30" rows="10"
                                          required
                                          data-parsley-required-message="برجاء إدخال التفاصيل"></textarea>
                                    @include('errors.validation-messages', ['field' => 'body'])
                                </div><!-- col -->
                            </div>
                        </div><!-- modal-body -->
                        <div class="modal-footer" style="direction: rtl">
                            <button type="submit" class="btn btn-primary tx-size-xs"
                            >إرسال
                            </button>
                            <button type="button" class="btn btn-secondary tx-size-xs"
                                    data-dismiss="modal">غلق
                            </button>
                        </div>
                    </form>
                </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->
    @endcan

@endsection
@section('scripts')
    <script>
        $('#sendNotification').parsley();
    </script>
@stop