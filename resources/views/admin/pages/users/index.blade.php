@extends('admin.layouts.master')

@section('title')

    المستخدمين

@stop

@section('content')

    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{route('admin.dashboard.index')}}">الرئيسيه</a>
            {{--                <a class="breadcrumb-item" href="{{route('user.showindex')}}">الفئات</a>--}}
            <span class="breadcrumb-item active">المستخدمين</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">جدول المستخدمين </h4>
        <p class="mg-b-0">عرض كل المستخدمين </p>
    </div>

    <div class="br-pagebody">
        <div class="br-section-wrapper">

            <div class="col-md-6"></div>
            <div class="table-wrapper">
                <table id="datatable1" class=" display  nowrap" style="width: 100%">
                    <thead>
                    <tr>
                        <th class="wd-15p all">الاسم</th>
                        <th class="wd-15p all">الهاتف</th>
                        <th class="wd-15p all">البريد الالكترونى</th>
                        <th class="wd-15p all">الحاله</th>
                        <th class="wd-15p all">التفعيل</th>
                        <th class="wd-20p all">المزيد</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as  $user)
                        <tr id="{{$user->id}}">
                            <td>{{$user->name}}</td>
                            <td>{{$user->phone}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->is_active ? 'نشط' : 'معلق'}}</td>
                            <td>{{$user->activated ? 'مفعل' : 'غير مفعل'}}</td>
                            <td>
                                <a href="{{route('admin.users.user.details' , $user->id)}}"
                                   class="btn btn-success btn-icon mg-r-5 mg-b-10">
                                    <div><i class="fa fa-arrow-left"></i></div>
                                </a>
                                @can('delete_users')
                                    <a href="#"
                                       onclick="showDeletConfirmationModal('{{route('admin.users.user.delete', $user->id)}}'  , {{$user->id}})"
                                       class="btn btn-danger btn-icon mg-r-5 mg-b-10">
                                        <div><i class="fa fa-trash-o"></i></div>
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
    </div><!-- br-pagebody -->

@stop

