@extends('admin.layouts.master')

@section('title')

    الصلاحيات

@stop
@section('styles')
    <link href="{{ asset('cpanel/css/bootstrap4-toggle.css')}}" media="all" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    <div class="br-profile-page">
        <div class="card shadow-base bd-0 rounded-0 widget-4">
            <div class="card-header ht-75">
                <div class="hidden-xs-down"></div>
                <div class="tx-24 hidden-xs-down"></div>
            </div><!-- card-header -->
        </div><!-- card -->

        <div class="ht-70 bg-gray-100 pd-x-20 d-flex align-items-center justify-content-center shadow-base">
            <ul class="nav nav-outline active-info align-items-center flex-row" role="tablist">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#mainuserdata" role="tab">
                        الصلاحيات </a></li>

            </ul>
        </div>

        <div class="tab-content br-profile-body">
            <div class="tab-pane fade active show" id="mainuserdata">
                <div class="row">
                    <div class="col-lg-12 mg-t-30 mg-lg-t-0">
                        {{--                         users --}}
                        <div class="row mg-b-20">
                            <div id="msdfopwe" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#msdfopwe" href="#1456wetpl"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                المستخدمين
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="1456wetpl" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض"
                                                       name="permissions[]"
                                                       value="show_users" data-off="عرض" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('show_users'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="تعديل"
                                                       name="permissions[]"
                                                       value="edit_users" data-off="تعديل" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('edit_users'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="حذف"
                                                       name="permissions[]"
                                                       value="delete_users" data-off="حذف" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('delete_users'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                         delegates --}}
                        <div class="row mg-b-20">
                            <div id="zxczxcqwe" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#msdfopwe" href="#1456wecxvcregtpl"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                المندوبين
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="1456wecxvcregtpl" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض"
                                                       name="permissions[]"
                                                       value="show_delegates" data-off="عرض" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('show_delegates'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="إضافه"
                                                       name="permissions[]"
                                                       value="add_delegates" data-off="إضافه" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('add_delegates'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="تعديل"
                                                       name="permissions[]"
                                                       value="edit_delegates" data-off="تعديل" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('edit_delegates'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="حذف"
                                                       name="permissions[]"
                                                       value="delete_delegates" data-off="حذف" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('delete_delegates'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                        admins--}}
                        <div class="row mg-b-20">
                            <div id="3j4khopwer" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#3j4khopwer" href="#0ytuiopt"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                المشرفين
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="0ytuiopt" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض"
                                                       name="permissions[]"
                                                       value="show_admins" data-off="عرض" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('show_admins'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="إضافه"
                                                       name="permissions[]"
                                                       value="add_admins" data-off="إضافه" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('add_admins'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="تعديل"
                                                       name="permissions[]"
                                                       value="edit_admins" data-off="تعديل" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('edit_admins'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="حذف"
                                                       name="permissions[]"
                                                       value="delete_admins" data-off="حذف" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('delete_admins'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                        orders--}}
                        <div class="row mg-b-20">
                            <div id="accordionzcxx" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#accordionzcxx" href="#asdzxc"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                الطلبات
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="asdzxc" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض"
                                                       name="permissions[]"
                                                       value="show_orders" data-off="عرض" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('show_orders'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="تعديل"
                                                       name="permissions[]"
                                                       value="edit_orders" data-off="تعديل" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('edit_orders'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                         laundry owners --}}
                        <div class="row mg-b-20">
                            <div id="msdfopwejtrht" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#msdfopwe" href="#1456wetplouio"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                أصحاب المغسلات
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="1456wetplouio" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض"
                                                       name="permissions[]"
                                                       value="show_laundry_owner" data-off="عرض" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('show_laundry_owner'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="إضافه"
                                                       name="permissions[]"
                                                       value="add_laundry_owner" data-off="إضافه" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('add_laundry_owner'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="تعديل"
                                                       name="permissions[]"
                                                       value="edit_laundry_owner" data-off="تعديل" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('edit_laundry_owner'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="حذف"
                                                       name="permissions[]"
                                                       value="delete_laundry_owner" data-off="حذف" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('delete_laundry_owner'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                         laundries --}}
                        <div class="row mg-b-20">
                            <div id="msdfopwe2312asf" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#msdfopwe" href="#1456wthyujuetpl"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                 المغسلات
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="1456wthyujuetpl" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض"
                                                       name="permissions[]"
                                                       value="show_laundry" data-off="عرض" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('show_laundry'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="تعديل"
                                                       name="permissions[]"
                                                       value="edit_laundry" data-off="تعديل" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('edit_laundry'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                         chats --}}
                        <div class="row mg-b-20">
                            <div id="msdfopwsadasde" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#msdfopwe" href="#1456wetpzxczl"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                المحادثات
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="1456wetpzxczl" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض"
                                                       name="permissions[]"
                                                       value="chats" data-off="عرض" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('chats'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                        suggestions--}}
                        <div class="row mg-b-20">
                            <div id="zxc23" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#zxc23" href="#hgdfe"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                المقترحات
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="hgdfe" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض"
                                                       name="permissions[]"
                                                       value="show_suggestions" data-off="عرض" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('show_suggestions'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="حذف"
                                                       name="permissions[]"
                                                       value="delete_suggestions" data-off="حذف" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('delete_suggestions'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                        notifications--}}
                        <div class="row mg-b-20">
                            <div id="zxctyuya" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#zxctyuya" href="#0sd5wea"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                الإشعارات
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="0sd5wea" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض"
                                                       name="permissions[]"
                                                       value="show_notification" data-off="عرض" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('show_notification'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="إضافه"
                                                       name="permissions[]"
                                                       value="add_notification" data-off="إضافه" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('add_notification'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                        settings--}}
                        <div class="row mg-b-20">
                            <div id="2d1fpeorez" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#2d1fpeorez" href="#0e83sad47h"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                الإعدادات
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="0e83sad47h" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">

                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="تعديل"
                                                       name="permissions[]"
                                                       value="edit_settings" data-off="تعديل" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('edit_settings'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                        cities--}}
                        <div class="row mg-b-20">
                            <div id="xz0wer87sd" class="accordion" role="tablist" aria-multiselectable="true"
                                 style="width: 100%">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mg-b-0">
                                            <a data-toggle="collapse" data-parent="#xz0wer87sd" href="#collapseOnezx"
                                               aria-expanded="true" aria-controls="collapseOne"
                                               class="tx-gray-800 transition">
                                                المدن
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->
                                    <div id="collapseOnezx" class="collapse " role="tabpanel"
                                         aria-labelledby="headingOne">
                                        <div class="card-block pd-20 row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض"
                                                       name="permissions[]"
                                                       value="show_cities" data-off="عرض" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('show_cities'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="إضافه"
                                                       name="permissions[]"
                                                       value="add_cities" data-off="إضافه" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('add_cities'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="تعديل"
                                                       name="permissions[]"
                                                       value="edit_cities" data-off="تعديل" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('edit_cities'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="role" data-toggle="toggle" data-on="حذف"
                                                       name="permissions[]"
                                                       value="delete_cities" data-off="حذف" data-onstyle="success"
                                                       data-offstyle="danger"
                                                       @if ($admin->hasPermissionTo('delete_cities'))
                                                       checked
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        </div>
                        {{--                        news--}}
                            <div class="row mg-b-20">
                                <div id="lkgdsa" class="accordion" role="tablist" aria-multiselectable="true" style="width: 100%">
                                    <div class="card">
                                        <div class="card-header" role="tab" id="headingOne">
                                            <h6 class="mg-b-0">
                                                <a data-toggle="collapse" data-parent="#lkgdsa" href="#uiohjcb" aria-expanded="true"
                                                    aria-controls="collapseOne" class="tx-gray-800 transition">
                                                    الاخبار
                                                </a>
                                            </h6>
                                        </div><!-- card-header -->
                                        <div id="uiohjcb" class="collapse " role="tabpanel" aria-labelledby="headingOne">
                                            <div class="card-block pd-20 row">
                                                <div class="col-sm-3">
                                                    <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض" name="permissions[]"
                                                        value="show_news" data-off="عرض" data-onstyle="success" data-offstyle="danger" @if($admin->hasPermissionTo('show_news'))
                                                    checked
                                                    @endif
                                                    >
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="checkbox" class="role" data-toggle="toggle" data-on="إضافه" name="permissions[]"
                                                        value="add_news" data-off="إضافه" data-onstyle="success" data-offstyle="danger" @if($admin->hasPermissionTo('add_news'))
                                                    checked
                                                    @endif
                                                    >
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="checkbox" class="role" data-toggle="toggle" data-on="تعديل" name="permissions[]"
                                                        value="edit_news" data-off="تعديل" data-onstyle="success" data-offstyle="danger" @if($admin->hasPermissionTo('edit_news'))
                                                    checked
                                                    @endif
                                                    >
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="checkbox" class="role" data-toggle="toggle" data-on="حذف" name="permissions[]"
                                                        value="delete_news" data-off="حذف" data-onstyle="success" data-offstyle="danger" @if($admin->hasPermissionTo('delete_news'))
                                                    checked
                                                    @endif
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- card -->
                                    <!-- ADD MORE CARD HERE -->
                                </div><!-- accordion -->
                            </div>
                            {{--                        testimonials--}}
                                <div class="row mg-b-20">
                                    <div id="lkgoxcvdvdsa" class="accordion" role="tablist" aria-multiselectable="true" style="width: 100%">
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingOne">
                                                <h6 class="mg-b-0">
                                                    <a data-toggle="collapse" data-parent="#lkgoxcvdvdsa" href="#hjghjghgjgh" aria-expanded="true"
                                                        aria-controls="collapseOne" class="tx-gray-800 transition">
                                                        اراء العملاء
                                                    </a>
                                                </h6>
                                            </div><!-- card-header -->
                                            <div id="hjghjghgjgh" class="collapse " role="tabpanel" aria-labelledby="headingOne">
                                                <div class="card-block pd-20 row">
                                                    <div class="col-sm-3">
                                                        <input type="checkbox" class="role" data-toggle="toggle" data-on="عرض" name="permissions[]"
                                                            value="show_testimonials" data-off="عرض" data-onstyle="success" data-offstyle="danger" @if($admin->hasPermissionTo('show_testimonials'))
                                                        checked
                                                        @endif
                                                        >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="checkbox" class="role" data-toggle="toggle" data-on="إضافه" name="permissions[]"
                                                            value="add_testimonials" data-off="إضافه" data-onstyle="success" data-offstyle="danger" @if($admin->hasPermissionTo('add_testimonials'))
                                                        checked
                                                        @endif
                                                        >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="checkbox" class="role" data-toggle="toggle" data-on="تعديل" name="permissions[]"
                                                            value="edit_testimonials" data-off="تعديل" data-onstyle="success" data-offstyle="danger" @if($admin->hasPermissionTo('edit_testimonials'))
                                                        checked
                                                        @endif
                                                        >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="checkbox" class="role" data-toggle="toggle" data-on="حذف" name="permissions[]"
                                                            value="delete_testimonials" data-off="حذف" data-onstyle="success" data-offstyle="danger" @if($admin->hasPermissionTo('delete_testimonials'))
                                                        checked
                                                        @endif
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- card -->
                                        <!-- ADD MORE CARD HERE -->
                                    </div><!-- accordion -->
                                </div>
                    </div><!-- col-lg-4 -->

                </div><!-- row -->
            </div><!-- tab-pane -->
        </div><!-- br-pagebody -->


    </div>
@stop
@section('scripts')

    <script type="text/javascript" src="{{asset('cpanel/js/bootstrap4-toggle.js')}}"></script>

    <script>
        $(document).on('change', '.role', function () {
            let permission = $(this).val();
            let token = "{{csrf_token()}}";
            $.ajax({
                url: "{{route('admin.admins.toggle.permissions')}}",
                type: "PATCH",
                data: {token: token, permission: permission, id: "{{$admin->id}}"},
            });
        })
    </script>
@stop