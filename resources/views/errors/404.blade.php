@extends('site.layouts.master')
@section('content')
    <!-- page title -->
    <section class="page-title centred" style="background-image: url({{ asset('site/images/about/page-title.png') }});">
        <div class="container">
            <div class="content-box">
                <div class="title">
                    <h1>خطأ 404</h1>
                </div>
    
            </div>
        </div>
    </section>
    <!--End Page Title-->
    
    
    <!-- error section -->
    <section class="error-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 error-column">
                    <div class="error-content rtl">
                        <div class="error-title">4<i class="fa fa-frown-o" aria-hidden="true"></i>4</div>
                        <div class="title">للأسف هذه الصفحة غير متوفرة !</div>
                        <div class="button"><a href="{{ route('site.home.index') }}" class="btn-one">الرجوع للصفحة الرئيسية</a></div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 error-column">
                    <div class="img-box centred">
                        <figure><img src="{{ asset('site/images/home/error.png') }}" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- error section end -->
@endsection