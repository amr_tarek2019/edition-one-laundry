$(document).ready(function () {
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = $('#min').datepicker("getDate");
            var max = $('#max').datepicker("getDate");
            var startDate = new Date(data[1]);
            var endDate = new Date(data[1]);
            endDate.setHours(0, 0, 0);
            if (min == null && max == null) {
                return true;
            }
            if (min == null && endDate <= max) {
                return true;
            }
            if (max == null && startDate >= min) {
                return true;
            }
            if (startDate >= min && endDate <= max) {
                return true;
            } else {
                return false;
            }
        }
    );

    $("#min").datepicker({
        onSelect: function () {
            table.draw();
        }, changeMonth: true, changeYear: true
    });
    $("#max").datepicker({
        onSelect: function () {
            table.draw();
        }, changeMonth: true, changeYear: true
    });
    'use strict';
    if ($('.select2') != null) {
        $('.select2').select2({
            minimumResultsForSearch: Infinity
        });
    }

    var table = $('#orders_table').DataTable({
        bLengthChange: false,
        responsive: true,
        "scrollX": true,
        language: {
            searchPlaceholder: ' ابحث هنا ...',
            sSearch: '',
            // "columnDefs": [
            //     { "searchable": false, "targets": 0 }
            // ]

        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: ['.print']
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ['.print']
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: ['.print']
                }
            },

        ]

    });
    table.column(4).visible(false);
    table.column(6).visible(false);
    // table.column( 4 ).search( false );
    // table.column( 6 ).search( false );
    // Event listener to the two range filtering inputs to redraw on input
    $('#min, #max').change(function () {
        table.draw();
    });
});