/**
 * This JS file applies only for messenger page.
 */
;(function () {
    var take = (messagesCount < 20) ? 0 : 20, // represents the number of messages to be displayed.
        $messenger = $('.messenger'),
        $loader = $('#messages-preloader'),
        channel = pusher.subscribe('messenger-channel');

    /**
     * Do action when a message event is triggered.
     */
    channel.bind('messenger-event', function (data) {
        if (data.senderId == withId && data.withId == authId && data.withId != data.senderId) { // current conversation thread.
            newMessage(data.message, 'received');
            playTweet();
            // makeSeen(authId, withId);
        } else if (data.withId == authId && data.withId != data.senderId) { // not opened thread.
            playTweet();
            // loadThreads();
        }
    });


    /**
     * Delete confirmation.
     */
    function confirmDelete() {
        return confirm('Are your sure you want to delete this message');
    }


    /**
     * Append a new message to chat body.
     */
    function newMessage(message, messageClass, failed = 0) {
        $('.messenger-body').append('\
            <div class="row message-row">\
                <p class="' + messageClass + '">' + message.message + '</p>\
            </div>\
        ');
        if (failed) {
            $('.messenger-body').append('\
                <a class="unsent">\
                    <small>\
                        This message didn\'t send. Check your internet connection and try again.\
                    </small>\
                </a><br>\
            ');
        } else {
            $('#message-body').val('');
        }
        scrollMessagesDown();
    }

    /**
     * Scroll messages down to some height or bottom.
     */
    function scrollMessagesDown(height = 0) {
        var scrollTo = height || $messenger.prop('scrollHeight');

        $messenger.scrollTop(scrollTo);
    }

    /**
     * Load more messages.
     */
    function loadMessages() {
        $.ajax({
            url: 'admin/messenger/more/messages',
            method: 'GET',
            data: {
                withId: withId,
                take: take
            }
        }).done(function (res) {
            var prevHeight = $messenger.prop('scrollHeight');

            $('.messenger-body').html(res.view);
            var newHeight = $messenger.prop('scrollHeight');
            scrollMessagesDown(newHeight - prevHeight); // stop at the current height.
            if (res.messagesCount < take) { // load no more messages.
                take = 0;
                $loader.after('<p class="start-conv">Conversation started</p>');
                $loader.remove();
            }
        });
    }

    /**
     * Play message notification sound.
     */
    function playTweet() {
        var audio = new Audio('/vendor/messenger/sounds/tweet.mp3');
        audio.play();
    }

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        scrollMessagesDown();


        function sendingMessage() {
            var message = $('#message-body').val();

            if (message) {
                var JqHXR = $.ajax({
                    url: '/admin/messenger/send',
                    method: 'POST',
                    data: {
                        message: message,
                        withId: withId
                    }
                });
            }
            JqHXR.done(function (res) { // message sent.
                if (res.success) {
                    newMessage(res.message, 'sent');
                    // loadThreads();
                }
            });
            JqHXR.fail(function (res) { // message didn't send.
                newMessage(res.message, 'sent', true);
            });
        }

        $(document).on('keypress', function (e) {
            if (e.which == 13) {
                sendingMessage();
            }
        });
        /**
         * Send message to backend and handle responses.
         */
        $(document).on('click', '#send-btn', function (e) {
            sendingMessage();
        });

        /**
         * Load more messages when scroll to top.
         */
        $messenger.on('scroll', function (e) {
            if (!$messenger.scrollTop() && take) {
                take += 20;
                loadMessages();
            }
        });
    });
}());
