<?php

use App\Admin;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Admin::class, 1)->create();
        \App\Setting::create([
            'about' => \Faker\Provider\Lorem::text(300),
            'terms' => \Faker\Provider\Lorem::text(300),
        ]);
//         $this->call(LaundrySeeder::class);
        $this->call(PermissionsTableSeeder::class);

    }
}
