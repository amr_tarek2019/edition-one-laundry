<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'show_admins',
            'add_admins',
            'edit_admins',
            'delete_admins',
            'show_delegates',
            'add_delegates',
            'edit_delegates',
            'delete_delegates',
            'show_laundry_owner',
            'add_laundry_owner',
            'edit_laundry_owner',
            'delete_laundry_owner',
            'show_laundry',
            'edit_laundry',
            'show_users',
            'edit_users',
            'delete_users',
            'show_orders',
            'edit_orders',
            'show_suggestions',
            'delete_suggestions',
            'show_notification',
            'add_notification',
            'edit_settings',
            'show_cities',
            'add_cities',
            'edit_cities',
            'delete_cities',
            'chats',
        ];
        foreach ($permissions as $permission) {
            \Spatie\Permission\Models\Permission::create([
                'name' => $permission,
                'guard_name' => 'admin'
            ]);
        }
        \App\Admin::first()->givePermissionTo(\Spatie\Permission\Models\Permission::all());
    }
}
