<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\LaundryServices::class, function (Faker $faker) {
    return [
        'laundry_id' => $faker->randomElement(\App\LaundryOwner::pluck('id')->toArray()),
        'name' => $faker->name,
        'wash' => $faker->randomElement([null, $faker->numberBetween(5, 100)]),
        'ironing' => $faker->randomElement([null, $faker->numberBetween(5, 100)]),
        'section_id' => $faker->numberBetween(1,4),

    ];
});
