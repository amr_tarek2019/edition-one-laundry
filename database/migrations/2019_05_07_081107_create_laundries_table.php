<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaundriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laundries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('owner_id');
            $table->string('section_id')->comment('1 men , 2 women , 3 sajad , 4 covers');
            $table->foreign('owner_id')->references('id')->on('laundry_owners')->onDelete('cascade');
            $table->string('name');
            $table->text('description');
            $table->string('img')->nullable();
            $table->string('lat');
            $table->string('lng');
            $table->string('address');
            $table->tinyInteger('has_offers');
            $table->string('phone');
            $table->string('email');
            $table->tinyInteger('is_active')->default(0);
            $table->date('expire_date');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laundries');
    }
}
