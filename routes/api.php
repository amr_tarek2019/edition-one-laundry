<?php

/*
* api Routes
*/
Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {

    // Auth Routes
    Route::group(['prefix' => 'auth', 'as' => 'auth.', 'namespace' => 'Auth'], function () {

        Route::post('register', ['uses' => 'AuthController@register', 'as' => 'register']);
        Route::post('login', ['uses' => 'AuthController@login', 'as' => 'login']);
        Route::post('code-confirmation', ['uses' => 'AuthController@codeConfirmation', 'as' => 'code.confirmation']);
        Route::post('reset-password', ['uses' => 'AuthController@resetPassword', 'as' => 'reset.password']);
        Route::post('change-password', ['uses' => 'AuthController@changePassword', 'as' => 'change.password']);

    });

    // settings Routes
    Route::group(['prefix' => 'settings'], function () {

        Route::apiResource('', 'SettingsController');

    });

    // Cities Routes
    Route::group(['prefix' => 'cities'], function () {

        Route::apiResource('', 'CitiesController');

    });


    // The  Routes That needs to be authenticated for

    Route::group(['middleware' => 'apiUserJWT'], function () {

        // chat Routes
        Route::group(['prefix' => 'chat'], function () {
            Route::post('send', 'ChatController@store');

        });

        // laundries Routes
        Route::group(['prefix' => 'laundries'], function () {

            Route::apiResource('', 'LaundryController');

        });
        // orders Routes
        Route::group(['prefix' => 'orders'], function () {
            Route::apiResource('', 'OrdersController');
            Route::post('cancel', 'OrdersController@update');
            Route::get('my-orders', ['uses' => 'OrdersController@myOrders']);
            Route::post('rate', 'OrdersController@rate');
            Route::post('fast-Order', 'OrdersController@fastOrder');
            Route::get('order-details','OrdersController@fastOrderDetails');
        });
        // profile Routes
        Route::group(['prefix' => 'profile'], function () {

            Route::post('update-info', 'ProfileController@updateInfo');
            Route::post('update-password', 'ProfileController@updatePassword');
            Route::post('update-notification-token', 'ProfileController@updateNotificationToken');
            Route::post('add-address', 'ProfileController@addAddress');
            Route::get('get-notification', 'ProfileController@getNotifications');


        });

        // Suggestions Routes
        Route::group(['prefix' => 'suggestions', 'as' => 'suggestions.'], function () {

            Route::post('add', ['uses' => 'SuggestionsController@add', 'as' => 'add']);

        });

    });



// Delegates

    Route::group(['namespace' => 'Delegate', 'prefix' => 'delegate'], function () {
        // Auth Routes
        Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {

//            Route::post('register', ['uses' => 'AuthController@register', 'as' => 'register']);
            Route::post('login', ['uses' => 'AuthController@login', 'as' => 'login']);
            Route::post('code-confirmation', ['uses' => 'AuthController@codeConfirmation', 'as' => 'code.confirmation']);
            Route::post('reset-password', ['uses' => 'AuthController@resetPassword', 'as' => 'reset.password']);
            Route::post('change-password', ['uses' => 'AuthController@changePassword', 'as' => 'change.password']);

        });

        // routes that needs authenticated user
        Route::group(['middleware' => 'apiDelegateJWT'], function () {
            // chat Routes
            Route::group(['prefix' => 'chat'], function () {

                Route::post('send', 'ChatController@store');
             
            });
            // orders routes

            Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {

                Route::get('get', ['uses' => 'OrdersController@getOrders', 'as' => 'orders']);
                Route::post('accept', ['uses' => 'OrdersController@acceptOrder', 'as' => 'accept.order']);
                Route::get('my-orders', ['uses' => 'OrdersController@myOrders', 'as' => 'my.orders']);
                Route::post('change-order-status', 'OrdersController@changeStatus');
 Route::get('order-details', ['uses' => 'OrdersController@fastOrderDetails', 'as' => 'my.order']);
            });

            // profile Routes
            Route::group(['prefix' => 'profile'], function () {

                Route::post('update-info', 'ProfileController@updateInfo');
                Route::post('update-password', 'ProfileController@updatePassword');
                Route::post('update-notification-token', 'ProfileController@updateNotificationToken');
                Route::get('get-notification', 'ProfileController@getNotifications');

            });

            // Suggestions Routes
            Route::group(['prefix' => 'suggestions', 'as' => 'suggestions.'], function () {

                Route::post('add', ['uses' => 'SuggestionsController@add', 'as' => 'add']);

            });
        });
    });

});
